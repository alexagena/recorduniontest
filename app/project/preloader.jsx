//framework
import Detections from '~fr/utils/Detections.js';
import ResizeController from '~fr/controllers/ResizeController.js';
import NavigationLoader from '~fr/navigation/NavigationLoader.js';
//relative
import styles from './preloader.scss';

/*
 * Preloader
 *
 */
class Preloader {
	
	constructor () {
		//params
		this._container = document.body;
				
		//loading
		this._loader = app.loader = new NavigationLoader();
		this._loader.on(NavigationLoader.START, this._startPreloader.bind(this));
		this._loader.on(NavigationLoader.PROGRESS, this._updatePercent.bind(this));
		this._loader.on(NavigationLoader.COMPLETE, this._preloaderFinished.bind(this));
		this._loader.start();
	}

	// Public
	//-------------------------------------------------------
	_startPreloader () {
		
	}
	
	_updatePercent (e, percent) {
		
	}

	_preloaderFinished (e) {
		app.trigger("preloadcomplete");
	}
}

//auto initiate
var __preloader = new Preloader();