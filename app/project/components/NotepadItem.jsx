//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import FormUtils from '~fr/utils/FormUtils.js';
import StringUtils from '~fr/utils/StringUtils.js';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import CircleButton from '~pr/assets/buttons/CircleButton.jsx';
//relative
import styles from './NotepadItem.scss';

/*
 * NotepadItem
 *
 */
export default class NotepadItem extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//default class
		this.state.full = false;
		this.addClass('notepad-item');
		
		//bind
		this._onDeleteRequested = this._onDeleteRequested.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
			text: PropTypes.string.isRequired,
			createdTime: PropTypes.number,
			delay: PropTypes.number,
			validations: PropTypes.array,
			startVisible: PropTypes.bool,
			onItemResize: PropTypes.func,
			onDeleteRequested: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			delay: 0,
			validations: []
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {text, createdTime} = this.props;
		let validate = this._validateText(text);
		let errorClass = validate.result ? "" : " has-error";

		return (
			<div ref="wrapper" class={this.state.className + errorClass}>
				<div ref="inner" class="pad-holder">
					<div class="rel-holder">
						<span class="time">{this._parseTime(createdTime)}</span>
						<h2 class="in-title" dangerouslySetInnerHTML={{__html: validate.message}} />
						<CircleButton class="close-button red" icon="close" onClick={this._onDeleteRequested} />
					</div>
				</div>
			</div>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		if(!this.props.startVisible) {
			this.hide(0);
			setTimeout(()=>this.show(), this.props.delay * 1000);
		}
	}

	// Public
	//-------------------------------------------------------
	hide (factor = 1, delay = 0) {
		TweenMax.killTweensOf(this.refs.wrapper);
		TweenMax.to(this.refs.wrapper, 0.8 * factor, {
			height: 0,
			force3D: true,
			delay: delay * factor,
			ease: Expo.easeInOut,
			onUpdate: factor == 0 ? null : this.props.onItemResize,
			onComplete: ()=> {
				this.hideComplete();
				this.refs.wrapper.style.display = "none";
			}
		});
	}
	
	show (factor = 1, delay = 0) {
		//get size
		this.refs.wrapper.style.height = "";
		this.refs.wrapper.style.display = "";
		let height = this.wrapper.getBoundingClientRect().height;
		this.refs.wrapper.style.height = 0;
		//animate
		if (height > 0) {
			TweenMax.killTweensOf(this.refs.wrapper);
			TweenMax.to(this.refs.wrapper, 0.8 * factor, {
				height: height,
				force3D: true,
				delay: delay * factor,
				ease: Expo.easeInOut,
				onUpdate: this.props.onItemResize,
				clearProps: "height, transform"
			});
			TweenMax.killTweensOf(this.refs.inner);
			TweenMax.to(this.refs.inner, 0.8 * factor, {
				startAt: {
					y: "125%"
				},
				y: "0%",
				force3D: true,
				delay: delay * factor,
				ease: Expo.easeInOut,
				clearProps: "transform",
				onComplete: ()=>this.hideComplete()
			});
		}
		else {
			this.refs.wrapper.style.height = "";
		}
	}

	// Private
	//-------------------------------------------------------
	_validateText (text) {
		let validate = FormUtils.validate(this.props.validations, text);
		if (!validate.result) {
			this.addClass("has-error");
			switch(validate.errorType) {
				case "noEmojis":
					return {
						result: false,
						message: "Emojis are not allowed"
					}
					break;

				case "maxText:100":
					return {
						result: false,
						message: "The maximum text size is 100. <br />" + StringUtils.limit(text, 100)
					}
					break;
			}
		}
		return {
			result: true,
			message: text
		}
	}

	_onDeleteRequested (e) {
		e.preventDefault();
		this.triggerEvent("onDeleteRequested");
	}

	_parseTime (createdTime) {
		let now = new Date(createdTime);
		let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		let date = StringUtils.leftPad(now.getDate(), 2);
		let month = StringUtils.leftPad(now.getMonth(), 2);
		let year = StringUtils.leftPad(now.getFullYear(), 4);
		let hours = StringUtils.leftPad(now.getHours(), 2);
		let minutes = StringUtils.leftPad(now.getMinutes(), 2);
		let seconds = StringUtils.leftPad(now.getSeconds(), 2);
		return `${date}/${month}/${year} - ${hours}:${minutes}:${seconds}`;
	}
}