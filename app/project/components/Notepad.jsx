//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import NotepadList from './NotepadList.jsx';
import NotepadForm from './NotepadForm.jsx';
import styles from './Notepad.scss';

/*
 * Notepad
 *
 */
export default class Notepad extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('notepad');
	}

	static get propTypes () {
		return {
			...super.propTypes,
			delay: PropTypes.number,
			messages: PropTypes.array,
			validations: PropTypes.array,
			listLimitHeight: PropTypes.number,
			onNoteSubmitted: PropTypes.func,
			onDeleteRequested: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {delay, messages, validations, listLimitHeight, onDeleteRequested, onNoteSubmitted} = this.props;
		return (
			<div ref="wrapper" class={this.state.className}>
				<NotepadList ref="list" delay={delay} messages={messages} limitHeight={listLimitHeight} validations={validations} onDeleteRequested={onDeleteRequested} />
				<NotepadForm onNoteSubmitted={onNoteSubmitted} validations={validations} />
			</div>
		);
	}
}