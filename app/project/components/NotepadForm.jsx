//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//project
import InputText from '~pr/assets/form/InputText.jsx';
import CircleButton from '~pr/assets/buttons/CircleButton.jsx';
//relative
import styles from './NotepadForm.scss';

/*
 * NotepadForm
 *
 */
export default class NotepadForm extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//default class
		this.addClass('notepad-form');

		//bind
		this._onSubmit = this._onSubmit.bind(this);
		this._onKeyDown = this._onKeyDown.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			validations: PropTypes.array,
			onNoteSubmitted: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {validations} = this.props;
		return (
			<div ref="wrapper" class={this.state.className}>
				<InputText
					ref="input"
					type="textarea"
					rows={2}
					maxLength={100}
					placeholder="Write your note here"
					validations={validations}
					onChange={this.validate.bind(this)}
					onKeyDown={this._onKeyDown}
				/>
				<CircleButton class="send-button" icon="send" onClick={this._onSubmit} />
			</div>
		);
	}
	
	// Public
	//-------------------------------------------------------
	validate () {
		let validate = this.refs.input.validate();
		this[validate.result ? "removeClass" : "addClass"]("form-error");
		return validate.result;
	}
	
	// Private
	//-------------------------------------------------------
	_onKeyDown (e) {
		if (e.nativeEvent.keyCode === 13) {
			e.preventDefault();
			this._onSubmit();
		}
	}
	
	_onSubmit () {
		if (this.validate()) {
			this.triggerEvent("onNoteSubmitted", {}, this.refs.input.value);
			this.refs.input.clear();
		}
	}
}