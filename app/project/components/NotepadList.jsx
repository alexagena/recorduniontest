//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import ScrollWrapper from '~fr/display/ScrollWrapper.jsx';
import ResizeController from '~fr/controllers/ResizeController.js';
//relative
import NotepadItem from './NotepadItem.jsx';
import styles from './NotepadList.scss';

/*
 * NotepadList
 *
 */
export default class NotepadList extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._scrolling = false;
		this._firstShow = true;

		//default class
		this.addClass('notepad-list');

		//bind
		this._onListResize = this._onListResize.bind(this);
		this._checkScrollHeight = this._checkScrollHeight.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			delay: PropTypes.number,
			messages: PropTypes.array,
			validations: PropTypes.array,
			limitHeight: PropTypes.number,
			onListResize: PropTypes.func,
			onDeleteRequested: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {messages} = this.props;
		let hasContent = messages && messages.length;
		
		return (
			<div ref="wrapper" class={this.state.className}>
				<ScrollWrapper ref="scroll" use3D={false}>
					{!!hasContent &&
						messages.map(this.renderItem.bind(this))
					}
					{!hasContent &&
						this.renderEmpty()
					}
				</ScrollWrapper>
			</div>
		);
	}
	
	renderItem (data, i) {
		return (
			<NotepadItem
				{...data}
				key={data.id}
				delay={this.props.delay}
				validations={this.props.validations}
				startVisible={this._firstShow}
				onItemResize={this._onListResize}
				onDeleteRequested={this.props.onDeleteRequested}
			/>
		);
	}

	renderEmpty () {
		return (
			<div class="empty-message">
				There are no items to show!
			</div>
		)
	}

	componentDidMount () {
		super.componentDidMount();
		this._firstShow = false;
		setTimeout(this._checkScrollHeight, 0);
		ResizeController.on(ResizeController.RESIZE, this._checkScrollHeight);
	}
	
	componentDidUpdate () {
		this._checkScrollHeight();
	}

	componentWillUnmount () {
		ResizeController.off(ResizeController.RESIZE, this._checkScrollHeight);
	}
	
	// Private
	//-------------------------------------------------------
	_onListResize () {
		this._checkScrollHeight();
		this.refs.scroll.scrollTo(0, 1);
		this.triggerEvent("onListResize");
	}

	_checkScrollHeight () {
		if(this.props.limitHeight) {
			let scroll = this.refs.scroll;
			let height = scroll.getContentHeight();
			let maxHeight = window.innerHeight * this.props.limitHeight;
			if (height > 0) {
				if (height > maxHeight) {
					scroll.wrapper.style.height = maxHeight + "px";
					scroll.updateScroll();
				}
				else {
					scroll.wrapper.style.height = "";
					scroll.updateScroll();
				}
			}
		}
	}
}