//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import styles from './BalloonContainer.scss';

/*
 * BalloonContainer
 *
 */
export default class BalloonContainer extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('balloon-container');
	}

	static get propTypes () {
		return {
			...super.propTypes,
			show: PropTypes.bool
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			show: false
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {children} = this.props;
		return (
			<div ref="wrapper" class={this.state.className}>
				<div class="balloon-content">{children}</div>
				<div ref="balloonArrow" class="arrow"></div>
			</div>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		this._checkBalloonState(this.props.show);
	}

	componentDidUpdate (prevProps, prevState) {
		this._checkBalloonState(this.props.show, prevProps.show);
	}

	// Private
	//-------------------------------------------------------
	_checkBalloonState (current, prev) {
		if(prev !== current) {
			let speed = prev == null ? 0 : 1;
			current ? this._showBalloon() : this._hideBalloon(speed);
		}
	}

	_showBalloon (factor = 1, delay = 0) {
		TweenMax.killTweensOf(this.refs.wrapper);
		TweenMax.to(this.refs.wrapper, 0.4 * factor, {
			startAt: {
				x: 0,
				y: -20,
				scale: 1,
				display: ""
			},
			y: 0,
			opacity: 1,
			force3D: true,
			delay: factor * delay,
			clearProps: "transform",
			ease: Back.easeOut
		});
		TweenMax.killTweensOf(this.refs.balloonArrow);
		TweenMax.to(this.refs.balloonArrow, 0.4 * factor, {
			startAt: {
				y: "-100%",
				x: "50%"
			},
			y: "-1%",
			x: "50%",
			force3D: true,
			delay: factor * delay,
			ease: Quart.easeInOut
		});
	}

	_hideBalloon (factor = 1, delay = 0) {
		TweenMax.killTweensOf(this.refs.wrapper);
		TweenMax.to(this.refs.wrapper, 0.4 * factor, {
			scale: 0.9,
			opacity: 0,
			force3D: true,
			delay: factor * delay,
			ease: Quart.easeInOut,
			transformOrigin: "80% 50%",
			onComplete: ()=>{
				TweenMax.set(this.refs.wrapper, {x: 999999});
			}
		});
	}
}