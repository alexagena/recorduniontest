//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//project
import CircleButton from '~pr/assets/buttons/CircleButton.jsx';
//relative
import BalloonContainer from './BalloonContainer.jsx';
import styles from './CollapsableBalloon.scss';

/*
 * CollapsableBalloon
 *
 */
export default class CollapsableBalloon extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.state.showBalloon = false;
		this.addClass('collapsable-balloon');
		
		//bind
		this._onBodyClick = this._onBodyClick.bind(this);
		this._toggleBalloon = this._toggleBalloon.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			children: PropTypes.object
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {children} = this.props;
		return (
			<div ref="wrapper" class={this.state.className}>
				<BalloonContainer ref="baloon" show={this.state.showBalloon}>
					{children}
				</BalloonContainer>
				<CircleButton icon="balloon" onClick={this._toggleBalloon} />
			</div>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		window.addEventListener("click", this._onBodyClick);
	}

	componentWillUnmount () {
		window.removeEventListener("click", this._onBodyClick);
	}

	// Public
	//-------------------------------------------------------
	showBalloon () {
		this.setState((state)=>{
			return {...state, showBalloon: true}
		});
	}

	hideBalloon () {
		this.setState((state)=>{
			return {...state, showBalloon: false}
		});
	}

	// Private
	//-------------------------------------------------------
	_toggleBalloon () {
		this.state.showBalloon ?
			this.hideBalloon() :
			this.showBalloon()
	}

	_onBodyClick (e) {
		if(e.defaultPrevented) {
			return;
		}
		if(e.target && !this.wrapper.contains(e.target)) {
			this.hideBalloon();
		}
	}
}