//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import StringUtils from '~fr/utils/StringUtils.js';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import ScrollWrapper from '~fr/display/ScrollWrapper.jsx';
//project
import Notepad from '~pr/components/Notepad.jsx';
import NotepadList from '~pr/components/NotepadList.jsx';
import CollapsableBalloon from '~pr/components/CollapsableBalloon.jsx';
import { newNote, deleteNote } from '~pr/store/notes';
//relative
import styles from './ScreenNotes.scss';

/*
 * ScreenNotes
 *
 */
export default class ScreenNotes extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._validations = ["required", "noEmojis", "maxText:100"];
		
		//default class
		this.addClass('screen-notes');

		//bind
		this._updateScroll = this._updateScroll.bind(this);
		this._onNoteSubmitted = this._onNoteSubmitted.bind(this);
		this._onItemDeleteRequested = this._onItemDeleteRequested.bind(this);
	}

	static get contextTypes () {
		return {
			store: PropTypes.object.isRequired
		}
	}

	static get propTypes () {
		return {
			...super.propTypes,
			messages: PropTypes.array
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			messages: []
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {messages} = this.props;
		
		return (
			<div ref="wrapper" class={this.state.className}>
				{/*Background list*/}
				<div class="main-note-list">
					<header class="list-header">
						<div class="h-wrapper">
							<h1 class="main-title">Notes List</h1>
						</div>
					</header>
					<ScrollWrapper ref="scroll" autoUpdate={true}>
						<div class="h-wrapper">
							<NotepadList
								delay={2}
								messages={messages}
								validations={this._validations}
								onListResize={this._updateScroll}
								onDeleteRequested={this._onItemDeleteRequested}
							/>
						</div>
					</ScrollWrapper>
				</div>
				
				{/*Dialog Notes list*/}
				<CollapsableBalloon icon="balloon">
					<Notepad
						delay={1}
						messages={messages}
						listLimitHeight={0.6}
						validations={this._validations}
						onNoteSubmitted={this._onNoteSubmitted}
						onDeleteRequested={this._onItemDeleteRequested}
					/>
				</CollapsableBalloon>
			</div>
		);
	}

	// Private
	//-------------------------------------------------------
	_updateScroll () {
		if(this.refs.scroll) {
			this.refs.scroll.updateScroll();
			this.refs.scroll.scrollTo(0, 1);
		}
	}

	_onNoteSubmitted (e, value) {
		if(value) {
			this.context.store.dispatch(newNote({
				id: StringUtils.createKey(),
				text: value,
				createdTime: Date.now()
			}));
		}
	}

	_onItemDeleteRequested (e) {
		if(e && e.target && e.target.props) {
			this.context.store.dispatch(deleteNote(e.target.props.id));
		}
	}
}