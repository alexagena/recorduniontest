import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ScreenNotes from './ScreenNotes';

/*
 * ScreenNotesContainer
 *
 */
class ScreenNotesContainer extends React.Component {
	render() {
		return (
			<ScreenNotes messages={this.props.messages} />
		);
	}
}

const mapStateToProps = function(state) {
	return {
		messages: state.notes.messages
	};
}

export default connect(mapStateToProps)(ScreenNotesContainer);