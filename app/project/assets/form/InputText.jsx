//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import FormUtils from '~fr/utils/FormUtils.js';
import BaseFormItem from '~fr/display/form/BaseFormItem.jsx';
//styles
import styles from './InputText.scss';

/*
 * InputText
 *
 */
export default class InputText extends BaseFormItem {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this._isOnEdit = null;
		this._selectionStart = 0;
		this._selectionEnd = 0;
		
		//state
		this.state.value = this._fixMaskValue(props.value, props.mask);
		this.addClass('input-text');

		//bind
		this._onChange = this._onChange.bind(this);
		this._onKeyDown = this._onKeyDown.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			mask: PropTypes.string,
			type: PropTypes.string,
			readOnly: PropTypes.bool,
			maxLength: PropTypes.number,
			placeholder: PropTypes.string,
			rows:  PropTypes.number,
			value: PropTypes.string,
			label: PropTypes.string,
			small: PropTypes.string,
			divide:  PropTypes.number,
			autoValidate: PropTypes.bool
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			type: "text",
			rows: 5,
			value: "",
			label: "",
			readOnly: false,
			placeholder: "",
			autoValidate: true
		}
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	get value () {return this.state.value;}
	set value (value) {
		if(typeof(value) !== 'string') {
			if(value != undefined) {
				value = value.toString();
			}
		}
		this._updateValue(value, false);
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let element;
		let inputProps = {
			style: {},
			name: this.props.name,
			type: this.props.type == "file" ? "text" : this.props.type,
			value: (this.state.value != null) ? this.state.value : (this.props.value || ""),
			readOnly: this.props.readOnly,
			placeholder: app.lang.tr(this.props.placeholder),
			onChange: this._onChange,
			onKeyDown: this._onKeyDown
		}
		//set max length
		if(this.props.maxLength != undefined) {
			inputProps.maxLength = this.props.maxLength;
		}
		//if is used as form.jsx child
		if(typeof(this.props.divide) == "number") {
			inputProps.style.width = (100 / this.props.divide) + "%";
		}
		//validate texarea type
		if(inputProps.type == "textarea") {
			element = <textarea ref="input" {...inputProps} rows={this.props.rows} />
		}
		else {
			element = <input ref="input" {...inputProps} />
		}
		//render
		return (
			<div ref="wrapper" class={this.state.className}>
				{element}
				{childs}
			</div>
		);
	}

	// Public
	//-------------------------------------------------------
	enable () {
		this._setDisable(false);
	}

	disable () {
		this._setDisable(true);
	}

	clear () {
		this.value = "";
		setTimeout(()=>this.setError(false), 100);
	}

	// Private
	//-------------------------------------------------------
	_onKeyDown (e) {
		this._selectionStart = e.target.selectionStart;
		this._selectionEnd = e.target.selectionEnd;
		this.triggerEvent("onKeyDown", e);
	}

	_onChange (e) {
		this._updateValue(e.target.value);
	}

	_setDisable (value = false) {
		if(value) {
			this.isDisabled = value;
			this.addClass('disabled');
			this.refs.input.setAttribute('disabled', true);
		}
		else {
			this.isDisabled = value;
			this.removeClass('disabled');
			this.refs.input.removeAttribute('disabled', true);
		}
	}

	_updateValue (value, trigger = true) {
		//fix mask
		value = this._fixMaskValue(value, this.props.mask);
		//update state
		this.setState({value});
		//delayed trigger event to get value after state change
		setTimeout(()=>{
			if(trigger) {
				this.triggerEvent("onChange", {}, value);
			}
			if(this._fixedSelStart != null && typeof(this.refs.input.selectionEnd) == "number") {
				this.refs.input.selectionEnd = this._fixedSelStart;
				this.refs.input.selectionStart = this._fixedSelStart;
				this._fixedSelStart = null;
			}
			this.props.autoValidate && this.validate();
		});
	}
	
	_fixMaskValue (value = "", mask) {
		let oldValue = this.state.value;
		let maskResult = FormUtils.mask(mask, value);
		let countDiffChars = this._countDiffBeforeStart(value, maskResult.value, this._selectionStart + (value.length - oldValue.length));
		let selStart = 0;
		//when add
		if(value.length > oldValue.length) {
			this._fixedSelStart = this._selectionEnd + (value.length - oldValue.length) + countDiffChars.length;
		}
		//when remove
		else {
			this._fixedSelStart = this._selectionEnd + (value.length - oldValue.length) + countDiffChars.length;
		}
		//trigger finish
		if(maskResult.finish) {
			this.triggerEvent("onTypeFinished", {});
		}
		return maskResult.value;
	}

	_countDiffBeforeStart (p_prev, p_next, start = 99) {
		let prev, next;
		if(p_next.length >= p_prev.length) {
			prev = p_prev;
			next = p_next;
		}
		else {
			prev = p_next;
			next = p_prev;
		}
		let prevI = 0
		let nextI = 0;
		let result = [];
		while(nextI < next.length) {
			if(prev[prevI] != next[nextI]) {
				if(nextI <= start) {
					result.push({i: nextI, char: next[nextI]});
				}
			}
			else {
				prevI++;
			}
			nextI++;
		}
		return result;
	}
}