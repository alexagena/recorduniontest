//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseButton from '~fr/display/BaseButton.jsx';
//project
import SVGIcon from '~fr/display/icons/SVGIcon.jsx';
//relative
import styles from './CircleButton.scss';

/*
 * CircleButton
 *
 */
export default class CircleButton extends BaseButton {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('circle-button');
	}

	static get propTypes () {
		return {
			...super.propTypes,
			icon: PropTypes.string
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {icon} = this.props;
		return super.render(
			<div class="button-holder">
				<SVGIcon ref="icon" data={icon} />
				<div class="hit-area" />
			</div>
		);
	}

	// Public
	//-------------------------------------------------------
	over (e) {
		super.over(e);
		if(this._enabled && !this._selected) {
			TweenMax.killTweensOf(this.refs.wrapper);
			TweenMax.to(this.refs.wrapper, 0.4, {
				scale: 1.1,
				force3D: true,
				ease: Back.easeOut
			});
			TweenMax.killTweensOf(this.refs.icon.wrapper);
			TweenMax.to(this.refs.icon.wrapper, 0.25, {
				scale: 0.7,
				rotation: -20,
				force3D: true,
				ease: Back.easeOut
			});
			TweenMax.to(this.refs.icon.wrapper, 0.2, {
				rotation: 0,
				delay: 0.25,
				force3D: true,
				ease: Back.easeOut
			});
		}
	}
	
	out (e) {
		super.out(e);
		if(this._enabled && !this._selected) {
			TweenMax.killTweensOf(this.refs.wrapper);
			TweenMax.to(this.refs.wrapper, 0.3, {
				scale: 1,
				force3D: true,
				ease: Back.easeOut,
				clearProps: "transform"
			});
			TweenMax.killTweensOf(this.refs.icon.wrapper);
			TweenMax.to(this.refs.icon.wrapper, 0.3, {
				scale: 1,
				rotation: 0,
				force3D: true,
				ease: Back.easeOut,
				clearProps: "transform"
			});	
		}
	}
}