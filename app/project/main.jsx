//third party
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import TweenMax from '~fr/third-party/TweenMax.js';
//framework
import Navigation from '~fr/navigation/Navigation.jsx';

import LightboxController from '~fr/controllers/LightboxController.jsx';
//project
import ViewsFactory from '~pr/views/ViewsFactory.js';
import ScreenNotesContainer from '~pr/components/ScreenNotesContainer.jsx';
//styles
import store from './store';
import styles from './main.scss';

/*
 * Main
 *
 */
class Main extends Navigation {
	
	constructor (props) {
		//call parent
		super(props, ViewsFactory);
		
		//state
		this.addClass("main-view");
	}

	// React
	//--------------------------------------------------------
	render () {
		//the content is data from main.json
		let {content} = this.props.config;

		//render
		return (
			<Provider store={store}>
				<div ref="wrapper" class={this.state.className}>
					{/*Render subviews*/}
					{this.renderManager()}

					{/*Everything that matters*/}
					<ScreenNotesContainer />
					
					{/*Div used to block user interaction*/}
					<div ref="blockScreen" class="block-screen" />
				</div>
			</Provider>
		);
	}
	
	// Common
	//-----------------------------
	lock () {
		this.refs.blockScreen.style.display = "block";
	}
	
	unlock () {
		this.refs.blockScreen.style.display = "none";
	}
}

/* 
 * Auto initiate
 * Wait the preloader and render Main
 * 
 */
(function () {
	
	function startNavigation () {
		//get main content
		let content = getMainContent(app.config);
		//remove listener
		app.off("preloadcomplete", startNavigation);
		//render main
		ReactDOM.render(<Main config={content} />, document.getElementById("app-root-container"));
	}
	
	function getMainContent (configFile) {
		var node = configFile.required.main;
		var content = null;
		var i = configFile.required.main.length;
		while (--i >= 0) {
			if(node[i].content) {
				node = node[i];
				break;
			}
		}
		return node;
	}
	
	//wait preloader to start
	app.on("preloadcomplete", startNavigation);
	
})();