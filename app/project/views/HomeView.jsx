//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseView from '~fr/display/BaseView.jsx';
//relative
import styles from './HomeView.scss';

/*
 * HomeView
 *
 */
export default class HomeView extends BaseView {

	constructor (props) {
		//call parent
		super(props);
		
		//default class
		this.addClass("home-view");
	}

	static get propTypes () {
		return {
			...super.propTypes
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}

	// React
	//-------------------------------------------------------
	render () {
		return (
			<div ref="wrapper" class={this.state.className}>
				
			</div>
		);
	}
}