//framework
import BaseFactory from '~fr/BaseFactory.js';
//factory
import HomeView from './HomeView.jsx';

/*
 * ViewsFactory
 *
 */
class ViewsFactory extends BaseFactory {

	constructor () {
		//call parent
		super();

		//classes list
		this.addComponent('home-view', HomeView);
	}
}

export default new ViewsFactory();