// types
//------------------------------------
export const NEW_NOTE = 'new-note';
export const DELETE_NOTE = 'delete-note';

// Default
//------------------------------------
const initalState = {
	messages: [
		{
			id: 1,
			text: "Lorem ipsum dolor sit amet, 😀 consectetur adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr",
			createdTime: Date.now()
		},
		{
			id: 2,
			text: "Sit amet, consectetur adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr",
			createdTime: Date.now()
		},
		{
			id: 3,
			text: "Adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr Adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr Adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr Adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr",
			createdTime: Date.now()
		},
		{
			id: 4,
			text: "Lorem ipsum dolor sit amet",
			createdTime: Date.now()
		},
		{
			id: 5,
			text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dolorem, est iure magni 14sdfsgdr",
			createdTime: Date.now()
		}
	]
}

// Actions
//------------------------------------
export const newNote = ({id, text, createdTime}) => {
	return {
		type: NEW_NOTE,
		payload: {id, text, createdTime}
	}
}

export const deleteNote = (id) => {
	return {
		type: DELETE_NOTE,
		payload: id
	}
}

// Helpers
//------------------------------------
let validateUniqueId = function (list, id) {
	for(let i = 0; i < list.length; i++) {
		if(list[i].id == id) {
			return false;
			break;
		}
	}
	return true;
}

// Store
//------------------------------------
export default (state = initalState, action) => {
	let {type, payload} = action;

	switch (type) {
		case NEW_NOTE:
			if(validateUniqueId(state.messages, payload.id)) {
				return {
					...state,
					messages: [payload, ...state.messages]
				}
			}
			else {
				throw new Error("Can1t duplicate id");
				return state;
			}
			break;

		case DELETE_NOTE:
			return {
				...state,
				messages: state.messages.filter((item)=>item.id !== payload)
			}
			break;

		default:
			return state;
	}
}