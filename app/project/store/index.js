import {createStore, combineReducers} from 'redux';
import notes from  './notes';

const Reducers = combineReducers({
	notes
});

const rootReducer = (state, action) => {
	return Reducers(state, action);
}

export default createStore(rootReducer);