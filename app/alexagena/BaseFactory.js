/*
 * Base factory
 * ~fr/BaseFactory.js
 */
export default class BaseFactory {

	constructor () {
		this._classes = [];
	}

	// Public
	//-------------------------------------------------------
	addComponent (key, klass) {
		this._classes.push([key, klass]);
	}

	make (data) {
		//Throw error when no class included
		if(!this._classes) {
			throw new Error("There are no items on Factory");
			return false;
		}
		
		//default klass
		let klass = this._classes[0][1];
		
		//get class from string
		if(typeof(data) == typeof('')) {
			klass = this._getKlassByName(data);
		}
		
		//if is an object and have a class param, get on classes object
		else if(typeof(data.class) == 'string') {
			klass = this._getKlassByName(data.class);
		}
		
		return klass;
	}

	// Private
	//-------------------------------------------------------
	_getKlassByName (name = '') {
		for(let klass of this._classes) {
			if(klass[0] == name) {
				return klass[1];
				break;
			}
		}
		//return default class
		return this._classes[0][1];
	}
}