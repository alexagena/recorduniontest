//framework
import App from '~fr/App.js';
import Router from '~fr/navigation/Router.js';
import GroupLoader from '~fr/loaders/GroupLoader.js';
import AssetLoader from '~fr/loaders/AssetLoader.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * NavigationLoader
 * Responsible to load all the assets
 *
 */
export default class NavigationLoader extends EventDispatcher {
	
	constructor () {
		//call parent
		super();
		
		//params
		this._configFile = app.baseURL + "data/config.json";
		this._router = new Router();
		this._loader = new AssetLoader();
		this._groups = [];
		this._groupIndex = 0;
		this._currentPhase = GroupLoader.PHASE_INITIAL;
		
		//bind
		this._onGroupLoaded = this._onGroupLoaded.bind(this);
		this._onConfigLoaded = this._onConfigLoaded.bind(this);
		this._onUpdateProgress = this._onUpdateProgress.bind(this);
	}
	
	static get START () {return "NavigationLoaderStart"}
	static get PROGRESS () {return "NavigationLoaderProgress"}
	static get COMPLETE () {return "NavigationLoaderComplete"}

	get router () {return this._router}

	// Public
	//-------------------------------------------------------
	start () {
		this._loadConfig();
	}
	
	// Private
	//----------------------------------------------------------
	_loadConfig () {
		var queue = new createjs.LoadQueue();
		queue.on('fileload', this._onConfigLoaded);
		queue.loadFile(this._configFile);
	}

	_onConfigLoaded (e) {
		//cache paths to replace
		app.paths = {...app.paths, ...e.result.paths};
		
		//setup project
		app.setup(e.result);
		
		//start router
		if(app.config.views.length) {
			this._router.config(app.config.views, app.paths);
			this._router.gotoInitialPath();
		}
		
		//cache, prepare and continue
		this._configFile = app.config;
		this._groups = this._prepareAllLoadingGroups(this._router.currentRouteData);
		this._loadNextGroup();
	}
	
	_prepareAllLoadingGroups (currentRouteData) {
		var groups = [];
		var configFile = this._configFile;
		
		//required files
		groups.push(this._getRequiredFiles('preloader'));
		groups.push(this._getRequiredFiles('main'));
		
		//views
		configFile.views.forEach((data)=>{
			//if page is active and content is set to download on background change to preloader step
			if(currentRouteData[0].id == data.id && data.contentLoadPhase == GroupLoader.PHASE_BACKGROUND) {
				data.contentLoadPhase = GroupLoader.PHASE_PRELOADER;
			}
			//create group and add to list
			let loader = this._loader.getGroup(data.id);
			loader.addFiles(data, GroupLoader.PHASE_INITIAL);
			groups.push(loader);
		});
		
		//return groups
		return groups;
	}
	
	_getRequiredFiles (key) {
		//list of items to load
		var phase = key == "preloader" ? GroupLoader.PHASE_INITIAL : null;
		var loader = this._loader.getGroup(key);
		var array = this._configFile.required[key];
		var filesList = [];
		
		//parse list and add on loader
		for(let i = 0; i < array.length; i++) {
			array[i].id = array[i].id || array[i].src;
			filesList.push(array[i]);
		}
		
		//add files to loader and return
		loader.addFiles(filesList, phase);
		return loader;
	}

	_loadNextGroup () {
		var current = this._groups[this._groupIndex];
		if (current) {
			current.on(GroupLoader.COMPLETE, this._onGroupLoaded);
			current.load(this._currentPhase);
		} else {
			this._groups.forEach((item)=>item.gotoNextPhase());
			this._phaseComplete();
		}
	}
	
	_onGroupLoaded (e) {
		e.target.off(GroupLoader.COMPLETE, this._onGroupLoaded);
		this._groupIndex++;
		this._loadNextGroup();
	}
	
	_phaseComplete (e, data) {
		if(this._currentPhase == GroupLoader.PHASE_INITIAL) {
			//watch preloader grup
			this._startPreloader();
			//restart group index and load the next phase
			this._groupIndex = 0;
			this._currentPhase = GroupLoader.PHASE_PRELOADER;
			this._loadNextGroup();
		}
		else if(this._currentPhase == GroupLoader.PHASE_PRELOADER) {
			//start main
			this._startMain();
			//continue loading next step
			this._groupIndex = 0;
			this._currentPhase = GroupLoader.PHASE_BACKGROUND;
			this._loadNextGroup();
		}
		else if(this._currentPhase == GroupLoader.PHASE_BACKGROUND) {
			
		}
	}
	
	_startPreloader () {
		let preloaderGroup = this._loader.getGroup('preloader');
		this._loader.on(AssetLoader.PROGRESS, this._onUpdateProgress);
		this._setupMultiLanguage(preloaderGroup);
		this.trigger(NavigationLoader.START);
	}

	_onUpdateProgress (e, [phase, percent]) {
		if(phase == GroupLoader.PHASE_PRELOADER) {
			this.trigger(NavigationLoader.PROGRESS, percent);
		}
	}
	
	_setupMultiLanguage (groupLoader, key = "language") {
		for(let i = 0; i < groupLoader.loadedFiles.length; i++) {
			let item = groupLoader.loadedFiles[i];
			//if key found
			if(item.id == key && typeof(item.result) == "object") {
				app.startMultiLanguage(item.result);
				break;
			}
		}
	}
	
	_startMain () {
		this._loader.off(AssetLoader.PROGRESS, this._onUpdateProgress);
		this.trigger(NavigationLoader.COMPLETE);
	}
}