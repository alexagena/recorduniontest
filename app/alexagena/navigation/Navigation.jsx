//framework
import Router from '~fr/navigation/Router.js';
import BaseView from '~fr/display/BaseView.jsx';

/*
 * Navigation
 *
 */
export default class Navigation extends BaseView {
	
	constructor (props = {}, ViewsFactory) {
		//validate required ViewsFactory
		if(!ViewsFactory) {
			throw new Error("ViewsFactory is required");
		}
		
		//call parent
		super(props);
		
		//create router
		this._router = new Router();
		this._currentView = null;
		this._previousView = null;
		this._lastChange = null;
		
		//globalize
		app.main = this;
		app.viewsFactory = ViewsFactory;
	}
	
	// Getters and Setters
	//----------------------------------------------------------
	static get START_CHANGE () {return 'NavigationStartChange';}
	static get CHANGED () {return 'NavigationChanged';}
	get router () {return this._router;}
	
	// React
	//----------------------------------------------------------
	componentDidMount() {
		super.componentDidMount();
		this.start();
	}
	
	// Public
	//----------------------------------------------------------
	start () {
		if(app.viewsFactory) {
			if(app.config.views.length) {
				this._router.on(Router.ROUTE_CHANGED, this.change.bind(this));
				this._router.config(app.config.views, app.paths);
				this._router.gotoInitialPath();
			}
		}
	}
	
	change (e, data) {
		let viewData = data[0];
		let routeData = data[1];
		
		//cache
		this._previousView = this._currentView;
		this._currentView = viewData;
		
		//get current view tree
		this._previousTree = this._getViewsTree(this._previousView);
		this._currentTree = this._getViewsTree(this._currentView);
		this._resultTree = this._intersectTree(this._previousTree, this._currentTree, routeData);

		//create views
		if(!this._resultTree.close.length && !this._resultTree.show.length) {
			if(this._currentView && this._currentView.ref) {
				this._updateViewRouteData(routeData);
			}
			else {
				this._lastChange = routeData;
			}
		}
		else {
			this._closeChain(this._resultTree.close, this._resultTree.show, routeData);
		}
		this._working = true;
	}
	
	// Private
	//----------------------------------------------------------
	_getViewsTree (view) {
		var list = [];
		if(view) {
			list.push(view);
			while(view.parentView) {
				view = this._getViewById(view.parentView);
				list.unshift(view);
			}
		}
		return list;
	}
	
	_getViewById (id) {
		for(let i = 0; i < app.config.views.length; i++) {
			if(app.config.views[i].id == id) {
				return app.config.views[i];
				break;
			}
		}
	}
	
	_intersectTree (a, b, routeData) {
		var toShow = [];
		var toClose = [];
		
		if(!a) {
			toShow = b;
			toClose = null;
		}
		else {
			for(let i = 0; i < b.length; i++) {
				let view = b[i];
				if(!a[i] || view.id != a[i].id) {
					toShow.push(view);
				}
			}
			for(let i = 0; i < a.length; i++) {
				let view = a[i];
				if(!b[i] || view.id != b[i].id) {
					toClose.unshift(view);
				}
			}
		}
		return {
			show: toShow,
			close: toClose
		}
	}
	
	_closeChain (toClose, toShow, routeData) {
		if(toClose && toClose.length) {
			var current = toClose.shift();
			var parent = this._getViewById(current.parentView) || {ref: this};

			//recursive close
			if(current.ref) {
				parent.ref.manager.close(current.ref, routeData, ()=>{
					//kill reference
					current.ref = null;
					delete current.ref;
					//continue
					this._closeChain(toClose, toShow, routeData);
				});
			}
			else {
				this._closeChain(toClose, toShow, routeData);
			}
		}
		else {
			this._showChain(toShow, routeData);
		}
	}

	_showChain (toShow, routeData) {
		if(toShow && toShow.length) {
			//show main if didnt already
			if(!this.showed) {
				this._showMain(toShow, routeData);
				return;
			}
			
			var current = toShow.shift();
			var parent = this._getViewById(current.parentView) || {ref: this};
			var Page = app.viewsFactory.make(current.class);
			
			//recursive show
			if(parent.ref) {
				parent.ref.manager.open(Page, current, routeData, ()=>{
					this._showChain(toShow, routeData);
				});
			}
		}
		else if (toShow.length <= 1) {
			this._working = false;
			if(this._lastChange) {
				this._updateViewRouteData(this._lastChange);
			}
		}
	}

	_showMain (toShow, routeData) {
		//listen show complete
		this.onShowComplete = ()=> {
			this._showChain(toShow, routeData);
		}
		//start show
		this.prepareToShow();
		this.show();
	}

	_updateViewRouteData (routeData) {
		if(routeData) {
			if(this._currentView.ref) {
				this._currentView.ref.updateRoute(routeData);
			}
		}
	}
}