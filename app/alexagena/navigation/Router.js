//third-party
import Ways from 'ways';
//framework
import PathUtils from '~fr/utils/PathUtils.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * Router
 *
 */
export default class Router extends EventDispatcher {

	constructor (data = {}) {
		//call parent
		super();
		
		//params
		this._data = data;
		this._paths = "";
		this._currentRouteData = {};
	}

	// Static
	//-------------------------------------------------------
	static get ROUTE_CHANGED () {return "RouterRouteChanged"}
	get currentRouteData () {return this._currentRouteData}

	// Public
	//-------------------------------------------------------
	config (views = [], paths) {
		//cache paths
		this._paths = paths;
		
		//update address bar
		Ways.use(Ways.addressbar);
		
		//config routes
		for(let i = 0; i < views.length; i++) {
			let viewHandler = this._getRouterHandler(views[i]);
			Ways(this._fixSlash(views[i].route), viewHandler);
			Ways(this._fixSlash(this._paths.projURL + views[i].route), viewHandler);
			Ways(this._fixSlash(this._paths.fullProjURL + views[i].route), viewHandler);
		}
	}
	
	gotoInitialPath () {
		let params = PathUtils.getParams();
		Ways.go(Ways.pathname() + params);
	}
	
	change (route, pageTitle, state) {
		route = this._fixRoute(route);
		route = this._fixSlash(this._paths.fullProjURL + route);
		Ways.go(route, pageTitle, state);
	}
	
	replace (route, pageTitle, state) {
		route = this._fixRoute(route);
		Ways.go.silent(route, pageTitle, state);
	}
	
	// Private
	//-------------------------------------------------------
	_getRouterHandler (viewData) {
		return (routeData)=> {
			routeData.query = PathUtils.getQueryObject();
			this._currentRouteData = [viewData, routeData];
			this.trigger(Router.ROUTE_CHANGED, this._currentRouteData);
		}
	}

	_fixRoute (route) {
		if(route) {
			var routeSplit = route.toString().split('?');
			var query = {
				...PathUtils.getQueryObject(),
				...PathUtils.parseQueryToObject(routeSplit[1])
			}
			query = PathUtils.parseObjectToQuery(query);
			route = routeSplit[0] + (query ? "?" + query : "");
		}
		return route;
	}

	_fixSlash (route) {
		if(typeof(route) == "string") {
			if(route[0] != "/") {
				route = "/" + route;
			}
			route = route.replace(/\/+/g, "/");
			route = route.replace(/\/+$/g, "");
			return route || "/";
		}
		return route;
	}
}