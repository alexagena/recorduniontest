//third party
import React from 'react';
import ReactDOM from 'react-dom';
//framework
import BaseView from '~fr/display/BaseView.jsx';
import DisplayController from '~fr/display/DisplayController.jsx';

/*
 * SyncViewsManager
 *
 */
export default class SyncViewsManager extends DisplayController {

	constructor (props) {
		//call parent
		super(props);
	}

	// Public
	//---------------------------------------
	open (Page, viewData, routeData, callback) {
		this._openCallback = callback;
		this.change(Page, {
			key: viewData.id,
			config: viewData,
			routeData: routeData
		});
	}

	close (view, routeData, callback) {
		this.removeContent(()=>{
			view = null;
			callback && callback();
		});
	}
			
	// Private
	//-------------------------------------------------------
	_changeFinished () {
		super._changeFinished();
		this._openCallback();
	}
}