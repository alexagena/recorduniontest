/*
 * PathUtils
 * ~fr/utils/PathUtils.js
 *
 */
export default class PathUtils {

	static getLocation () {
		var src;
		if(document.currentScript) {
			src = document.currentScript.src;
		}
		else {
			let doc = document.querySelector('#main-js');
			src = doc ? doc.getAttribute("src") : "";
		}
		src = PathUtils.toRelative(src);
		src = src.replace(/[^\/]*$/gi, '');
		src = src ? src : window.location.pathname;
		return src;
	}

	static getBase () {
		let baseTag = document.querySelector("base");
		if (baseTag) {
			let href = PathUtils.toRelative(baseTag.getAttribute("href"));
			return href || "";
		}
		return "/"; 
	}

	static toRelative (path) {
		return path.replace(/^((http[^\/]*\/\/)|(:?\/\/))[^\/]*/gi, '');
	}

	static getParams () {
		return window.location.search;
	}

	static getProjectPaths (url = location.href, base = "") {
		url = PathUtils.toRelative(url);
		url = url.replace(base, "");
		url = url.replace(/\/+/g, "/");
		return url;
	}

	static getQueryObject () {
		var query = window.location.search.substring(1);
		return PathUtils.parseQueryToObject(query);
	}

	static parseQueryToObject (query) {
		var result = {};
		if(query) {
			query = query.split('&');
			for(let i = 0; i < query.length; i++) {
				let item = query[i].split('=');
				result[item[0]] = item[1];
			}
		}
		return result;
	}

	static parseObjectToQuery (object = {}) {
		var result = "";
		for(let key in object) {
			if(object[key]) {
				result += key + '=' + object[key] + '&';
			}
		}
		result = result.replace(/&$/ig, "");
		return result;
	}

	static compare (a, b) {
		if(typeof(a) == "string" && typeof(b) == "string") {
			a = a.replace(/^\/+|\/+$/, "");
			b = b.replace(/^\/+|\/+$/, "");
			return a == b;
		}
		return false;
	}
}