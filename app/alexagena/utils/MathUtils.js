export default class MathUtils {
	
	static inRange (point, min, max, edge = false) {
		return point >= min && point < max;
	}
	
	static pointSquareIntersection (point, square) {
		return MathUtils.inRange(point.x, square.x, square.x + square.width) && 
			   MathUtils.inRange(point.y, square.y, square.y + square.height);
	}
	
	static rangeIntersection (min0, max0, min1, max1) {
		return max0 > min1 && min0 < max1;
	}
	
	static squareIntersection (s0, s1) {
		return MathUtils.rangeIntersection(s0.x, s0.x + s0.width, s1.x, s1.x + s1.width) && 
			   MathUtils.rangeIntersection(s0.y, s0.y + s0.height, s1.y, s1.y + s1.height);
	}
	
	static valueToPercent (value, ini, diff) {
		return (value - ini) / diff * 100;
	}

	static percentToValue (value, ini, diff) {
		return (percent * diff) + ini;
	}
	
	static gcd (a, b) {
		if (!b) return a;
		return MathUtils.gcd(b, a % b);
	}

	static greaterDivisor (pValue, pDivisor) {
		var value = pValue;
		var divisor = pDivisor;
		var rest = 0;

		while (divisor > 0) {
			if(value % divisor == 0) {
				break;
			}
			divisor--;
		}
		if(divisor == 1) {
			rest = pValue % pDivisor;
			value = pValue - rest;
			divisor = MathUtils.greaterDivisor(value, pDivisor);
		}
		return divisor;
	}

	static roundValue (value, factor = 1, func = Math.round) {
		let ddd = func(value * factor) / factor;
		return func(value * factor) / factor;
	}

	static zeroesLog (value) {
		return Math.pow(10, Math.floor(Math.log(value) / Math.LN10))
	}

	static smartRound (value, func = Math.round) {
		let factor = MathUtils.zeroesLog(value);
		if(factor) {
			value = MathUtils.roundValue(value, 1 / factor, func);
		}
		return value;
	}
}