import VMasker from 'vanilla-masker';

class Validations {

	test (method, value) {
		var methodData = method.split(':');
		var method = this[methodData[0]];

		if(typeof(method) == 'function') {
			methodData.shift();
			return method(value, methodData);
		}
		return true;
	}

	/*
	 * value = 1000
	 * params = "minNum:$int"
	 */
	minNum (value, params) {
		value = +value;
		let limit = +params[0];
		if(typeof(limit) == "number" && typeof(value) == "number") {
			return value >= limit;
		}
		return false;
	}

	/*
	 * value = 1000
	 * params = "maxNum:$int"
	 */
	maxNum (value, params) {
		value = +value;
		let limit = +params[0];
		if(typeof(limit) == "number" && typeof(value) == "number") {
			return value <= limit;
		}
		return false;
	}

	/*
	 * value = ""
	 * params = "minText:$int"
	 */
	minText (value, params) {
		value = value.length;
		let limit = +params[0];
		if(typeof(limit) == "number" && typeof(value) == "number") {
			return value >= limit;
		}
		return false;
	}

	/*
	 * value = ""
	 * params = "maxText:$int"
	 */
	maxText (value, params) {
		value = value.length;
		let limit = +params[0];
		if(typeof(limit) == "number" && typeof(value) == "number") {
			return value <= limit;
		}
		return false;
	}

	/*
	 * value = ""
	 * params = ""
	 */
	cpf (value, params) {
		var sum;
		var rest;
		value = value.replace(/\D+/g, '');
		sum = 0;
		if (value == "00000000000") return false;

		for (let i = 1; i<=9; i++) sum = sum + parseInt(value.substring(i-1, i)) * (11 - i);
		rest = (sum * 10) % 11;

		if ((rest == 10) || (rest == 11))  rest = 0;
		if (rest != parseInt(value.substring(9, 10)) ) return false;

		sum = 0;
		for (let i = 1; i <= 10; i++) sum = sum + parseInt(value.substring(i-1, i)) * (12 - i);
		rest = (sum * 10) % 11;

		if ((rest == 10) || (rest == 11))  rest = 0;
		if (rest != parseInt(value.substring(10, 11) ) ) return false;
		return true;
	}

	/*
	 * value = ""
	 * params = ""
	 */
	rg (value, params) {
		value = value.replace(/\D+/g, '');
		return value.length > 5;
	}

	/*
	 * value = ""
	 * params = "date:d-m-y"
	 */
	date (value, params = []) {
		let separator = value.replace(/\d+/g, '')[0] || "-";
		let dateArray = value = value.split(separator);
		let mask = (params[0] || "d-m-y").split('-');
		let day, month, year;
		for(let i = 0; i < mask.length; i++) {
			switch(mask[i][0]) {
				case "d":
					day = +dateArray[i];
					break;
				case "m":
					month = +dateArray[i] - 1;
					break;
				case "y":
					year = +dateArray[i];
					break;
			}
		}
		let date = new Date(year, month, day);
		return year == date.getFullYear() && month == date.getMonth() && day == date.getDate()
	}

	/*
	 * value = ""
	 * params = ""
	 */
	email (value, params = []) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(value);
	}

	/*
	 * value = ""
	 * params = ""
	 */
	phone (value) {
		value = value.replace(/\D+/g, '');
		return value.length >= 9;
	}

	/*
	 * value = ""
	 * params = ""
	 */
	cep (value) {
		value = value.replace(/\D+/g, '');
		return value.length >= 8;
	}

	/*
	 * value = ""
	 * params = ""
	 */
	noEmojis (value) {
		var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
		return !regex.test(value);
	}
	
	/*
	 * value = ""
	 * params = "required"
	 */
	required (value) {
		return value != null && value != "" && value != NaN;
	}
}

class Masks {

	apply (method, value) {
		if(this[method]) {
			return this[method](value);
		}
		else if(typeof(method) == "string") {
			return this.def(value, method);
		}
		return {value, finish: false};
	}

	numbers (value) {
		return {
			value: value.replace(/\D+/g, ''),
			finish: false
		}
	}

	letters (value) {
		return {
			value: value.replace(/\W+|\d+/g, ''),
			finish: false
		}
	}

	phone (value) {
		value = value.replace(/\D+/g, '');
		let mask = value.length >= 11 ? "(99) 9-9999-9999" : "(99) 9999-9999";
		let fixed = VMasker.toPattern(value, mask);
		return {
			value: fixed,
			finish: fixed.length == 16
		}
	}

	cpf (value) {
		return this.def(value, "999.999.999/99");
	}

	date (value) {
		return this.def(value, "99/99/9999");
	}

	cep (value) {
		return this.def(value, "99999-999");
	}

	def (value, mask) {
		let fixed = VMasker.toPattern(value, mask);
		return {
			value: fixed,
			finish: fixed.length == mask.length
		}
	}
}

class FormUtils {

	constructor () {
		this.masks = new Masks();
		this.validations = new Validations();
	}

	/*
	 * Get a list of methods like: [cpf, min:3, required]
	 * and test the value using Validations
	 *
	 */
	validate (list = [], value = "") {
		let result = true;
		let errorType = null;
		for(let i = 0; i < list.length; i++) {
			if(!this.validations.test(list[i], value)) {
				result = false;
				errorType = list[i];
				break;
			}
		}
		return {result, errorType}
	}

	mask (mask, value = "") {
		return this.masks.apply(mask, value);
	}
}

export default new FormUtils();