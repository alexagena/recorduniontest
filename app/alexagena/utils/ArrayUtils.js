export default class ArrayUtils {
	static contain (array, item) {
		var result = false;
		for(var i = 0; i < array.length; i++) {
			if(array[i] == item) {
				result = true;
				break;
			}
		}
		return result;
	}

	static remove (array, item) {
		array.forEach(function (value, i) {
			if(item == value) {
				array.splice(i, 1);
				return false;
			}
		});
	}
}