/*
 * StringUtils
 *
 */
export default class StringUtils {
	
	static replaceAccents (value) {
		if (value.search(/[\xC0-\xFF]/g) > -1) {
			value = value
				.replace(/[\xC0-\xC5]/g, "A")
				.replace(/[\xC6]/g, "AE")
				.replace(/[\xC7]/g, "C")
				.replace(/[\xC8-\xCB]/g, "E")
				.replace(/[\xCC-\xCF]/g, "I")
				.replace(/[\xD0]/g, "D")
				.replace(/[\xD1]/g, "N")
				.replace(/[\xD2-\xD6\xD8]/g, "O")
				.replace(/[\xD9-\xDC]/g, "U")
				.replace(/[\xDD]/g, "Y")
				.replace(/[\xDE]/g, "P")
				.replace(/[\xE0-\xE5]/g, "a")
				.replace(/[\xE6]/g, "ae")
				.replace(/[\xE7]/g, "c")
				.replace(/[\xE8-\xEB]/g, "e")
				.replace(/[\xEC-\xEF]/g, "i")
				.replace(/[\xF1]/g, "n")
				.replace(/[\xF2-\xF6\xF8]/g, "o")
				.replace(/[\xF9-\xFC]/g, "u")
				.replace(/[\xFE]/g, "p")
				.replace(/[\xFD\xFF]/g, "y");
		}
		return value;
	}

	static removeTags (text) {
		return text.replace(/<[^>]*>/gi, " ");
	}

	static slugIt (value, separator = '-') {
		return StringUtils.replaceAccents(value.replace(/\s+/gi, separator).toLowerCase());
	}

	static limit (value, length, sufix = '...') {
		if(!value || !length) {
			return value;
		}
		if(value.length <= length) {
			return value;
		}
		return value.substring(0, length) + sufix;
	}

	static leftPad (value, length = 2) {
		if(typeof(value) != "string") {
			value = value.toString();
		}
		let loop = length - value.length;
		let extraZeros = "";
		if(loop > 0) {
			for(let i = 0; i < loop; i++) {
				extraZeros += "0";
			}
		}
		return extraZeros + value; 
	}

	static rightPad (value, length = 2) {
		if(typeof(value) != "string") {
			value = value.toString();
		}
		let loop = length - value.length;
		let extraZeros = "";
		if(loop > 0) {
			for(let i = 0; i < loop; i++) {
				extraZeros += "0";
			}
		}
		return value + extraZeros; 
	}

	static createKey () {
		return (Date.now() * Math.random() * 0x100000).toString(16);
	}

	static smallName (name) {
		let result = name;
		let testName = StringUtils.replaceAccents(name);
		if(name.length >= 2) {
			let f = name[0];
			let spl = name.split(" ");
			let l = spl.length > 1 ? spl[1][0] : name[1];
			//avoid bad names
			let testRes = StringUtils.replaceAccents(f + l).toUpperCase();
			if(testRes == "CU") {
				for(let i = 1; i < testName.length; i++) {
					if(testName[i] && testName[i].toLowerCase() != "u") {
						l = name[i];
						break;
					}
				}
			}
			//result
			result = f + l;
		}
		return result.toUpperCase(); 
	}
}