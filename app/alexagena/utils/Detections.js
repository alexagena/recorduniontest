/*
 * Detections
 *
 */
class Detections {

	constructor () {
		this.ua = navigator.userAgent || '';
		this.orientation = window.innerWidth > window.innerHeight ? 'landscape' : 'portrait';
		this.touch = Boolean(window.hasOwnProperty("ontouchstart")) || Boolean(navigator.maxTouchPoints > 0) || Boolean(navigator.msMaxTouchPoints > 0);
		this.tablet = /(ipad.*|tablet.*|(android.*?chrome((?!mobi).)*))$/i.test(this.ua);
		this.mobile = !this.tablet && Boolean(this.getFirstMatch(/(ipod|iphone|ipad)/i, this.ua) || /[^-]mobi/i.test(this.ua));
		this.desktop = !this.mobile && !this.tablet;
		this.webgl = this.detectWebGLContext();
		this.cpu;
	}
	
	get cpu () {
		if(!window._cpuTest) {
			window._cpuTest = this.detectPerformance();
		}
		return window._cpuTest;
	}

	getFirstMatch (re, val) {
		let m = val.match(re)
		return (m && m.length > 1 && m[1]) || null;
	}

	detectWebGLContext () {
		if(typeof(this.webgl) !== "boolean") {
			//Create canvas element. The canvas is not added to the document
			//itself, so it is never displayed in the browser window
			var canvas = document.createElement("canvas");
			//Get WebGLRenderingContext from canvas element.
			var gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
			//Report the result.
			return gl && gl instanceof WebGLRenderingContext;
		}
		return this.webgl;
	}

	detectPerformance () {
		var _speedconstant = 1.15600e-8; //if speed=(c*a)/t, then constant=(s*t)/a and time=(a*c)/s
		var d = new Date();
		var amount = 150000000;
		var estprocessor = 1.7; //average processor speed, in GHZ
		for (var i = amount; i>0; i--) {} 
		var newd = new Date();
		var accnewd = Number(String(newd.getSeconds())+"."+String(newd.getMilliseconds()));
		var accd = Number(String(d.getSeconds())+"."+String(d.getMilliseconds())); 
		var di = accnewd-accd;
		if (d.getMinutes() != newd.getMinutes()) {
		di = (60*(newd.getMinutes()-d.getMinutes()))+di}
		let spd = ((_speedconstant*amount)/di);
		return {
			time: Math.round(di * 1000) / 1000,
			ghz: Math.round(spd * 1000) / 1000
		}
	}
}

export default new Detections();