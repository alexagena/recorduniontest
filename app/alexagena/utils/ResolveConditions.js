/*
 * ResolveConditions
 * @author Alex Agena
 *
 */
class ResolveConditions {
	
	// Public
	//----------------------------------------------------------
	test (expression, callback) {
		let inner = this.findInnerExpression(expression);
		let result = this.resolveExpression(inner, callback);
		//replace result in expression
		expression = expression.replace(inner, result);
		//check if expression is done
		if(expression !== "true" && expression !== "false") {
			return this.test(expression, callback);
		}
		//result
		return expression == "true";
	}
	
	findInnerExpression (expression) {
		let open = expression.lastIndexOf("(");
		let close = open + expression.substring(open, expression.length).indexOf(")");
		if(open != -1 && close > open) {
			return expression.substring(open, close + 1);
		}
		return expression;
	}
	
	resolveExpression (expression, callback) {
		//remove expression parenthesis
		expression = expression.replace(/^\(|\)$/g, "");
		expression = this.resolveSingleOperation(expression, callback);
		//test if expression is done
		if(expression !== "true" && expression !== "false") {
			return this.resolveExpression(expression, callback);
		}
		//result
		return expression == "true";
	}

	resolveSingleOperation (phrase, callback) {
		let match = phrase.match(/[^&\|]+(&&|\|\|)\s+[^\s]+/);
		let result;
		//if found expression like "a && b" or "a || b" 
		if (match) {
			let expression = match[0];
			let operation = match[1];
			let conditions = expression.split(operation);
			//inital value based on operation
			result = operation == "&&";
			//run into conditions
			for(let i = 0; i < conditions.length; i++) {
				let condResult = this.resolveCondition(conditions[i], callback);
				if (condResult && operation == "||") {
					result = true;
					break;
				}
				if (!condResult && operation == "&&") {
					result = false;
					break;
				}
			}
			return phrase.replace(expression, result);
		}
		//if expression has a single condition
		else {
			result = this.resolveCondition(phrase, callback);
			return phrase.replace(phrase, result);
		}
	}

	resolveCondition (condition, callback) {
		//remove spaces from condition
		condition = condition.trim();
		//if are solved values
		if(condition == "true" || condition == "false") {
			return condition == "true";
		}
		//send them to be tested
		else if (callback) {
			return callback(condition.trim());
		}
		return false;
	}
}

export default new ResolveConditions();