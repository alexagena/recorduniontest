//framework
import StringUtils from "~fr/utils/StringUtils.js";

/*
 * DateUtils
 *
 */
export default class DateUtils {
	
	static timeStringToMillisec (string, separator = ':') {
		if(!string) {
			return string;
		}
		var time = string.split(separator);
		var result = 0;
		result += +time[0] * 3600;
		result += +time[1] * 60;
		result += +time[2];
		return result * 1000;
	}

	static millisecToTimeString (value, separator = ':') {
		value = value || 0;
		var hour = StringUtils.leftPad(Math.floor(value / 3600 / 1000), 2);
		var minutes = StringUtils.leftPad(Math.floor(value % (3600 * 1000) / 60 / 1000), 2);
		var seconds = StringUtils.leftPad(Math.floor(value % (60 * 1000) / 1000), 2);
		return [hour, minutes, seconds].join(separator);
	}
}