//framework
import StringUtils from '~fr/utils/StringUtils.js';

/*
 * ColorUtils
 *
 */
class ColorsUtils {

	constructor () {
		this.dict = {};
	}

	// Public
	//-------------------------------------------------------
	sumColor (color, factor = 0) {
		let result = "#";
		for(let i = 0; i < color.length; i++) {
			let val = color[i];
			if(val != "#") {
				val = this._convertHexChar(val);
				val += factor;
				val = this._convertDecChar(val);
				result += val;
			}
		}
		return result;
	}

	isLightColor (c) {
		if(c) {
			if(this.dict.hasOwnProperty(c)) {
				return this.dict[c];
			}
			else {
				let color = c.substring(1); //strip #
				let rgb = parseInt(color, 16); //convert rrggbb to decimal
				let r = (rgb >> 16) & 0xff; //extract red
				let g = (rgb >> 8) & 0xff; //extract green
				let b = (rgb >> 0) & 0xff; //extract blue
				let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
				return this.dict[c] = luma > 100;
			}
		}
	}

	rgbToHex (value, useString = true) {
		let color = value.replace(/(rgba\()|(\))/gi, "").split(",");
		let r = +color[0];
		let g = +color[1];
		let b = +color[2];
		let a = +color[3];
		let result = StringUtils.leftPad(r.toString(16)) + StringUtils.leftPad(g.toString(16)) + StringUtils.leftPad(b.toString(16));

		if(useString) {
			return "#" + result;
		}
		else {
			return parseInt(result, 16);
		}
	}
	
	smartToHex (value) {
		if(/rgb/gi.test(value)) {
			return this.rgbToHex(value);
		}
		else if (/^#/gi.test(value)) {
			return this.rgbToHex(value);
		}
	}

	// Private
	//-------------------------------------------------------
	_convertHexChar (char) {
		let result = +char;
		if(Number.isNaN(result)) {
			char = char.toUpperCase();
			switch (char) {
				case "A":result = 10;break;
				case "B":result = 11;break;
				case "C":result = 12;break;
				case "D":result = 13;break;
				case "E":result = 14;break;
				case "F":result = 15;break;
				default: result = 0;break;
			}
		}
		return result;
	}

	_convertDecChar (num) {
		let hex = "0123456789ABCDEF";
		let index = ((num % hex.length) + hex.length) % hex.length;
		return hex[index];
	}

}

export default new ColorsUtils()