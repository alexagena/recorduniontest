export default class JSONUtils {

	static filterObject (data, name, type = null, ignore = null, getParent = false) {
		var resp = [];
		var name = [].concat(name);

		if(ignore) {
			ignore = [].concat(ignore);
		}

		for(var k in data) {
			var v = data[k];
			var add = false;
			if(ignore) {
				if(ignore.indexOf(k) >= 0) {
					continue;
				}
			}
			if(name.indexOf(k) >= 0) {
				add = true;
				if(type) {
					if(typeof(v) != type) {
						add = false;
					}
				}
				if(add) {
					if(getParent) {
						if(resp.indexOf(data) < 0) {
							resp.push(data);
						}
					} else {
						resp.push(v);
					}
				}
			}
			if(typeof(v) == 'array' || typeof(v) == 'object') {
				resp = [].concat(resp, JSONUtils.filterObject(v, name, type, ignore, getParent));
			}
		}
		return resp;
	}

	static replace (content, paths) {
		var isObject = typeof(content) == 'object';
		if(isObject) {
			content = JSON.stringify(content);
		}
		for(let key in paths) {
			let pattern = new RegExp("{" + key + "}", "g");
			content = content.replace(pattern, paths[key]);
		}
		if(isObject) {
			content = JSON.parse(content);
		}
		return content;
	}
}