//framework
import PathUtils from '~fr/utils/PathUtils.js';
import JSONUtils from '~fr/utils/JSONUtils.js';
import Prototypes from '~fr/Prototypes.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';
import LanguageController from '~fr/controllers/LanguageController.js';

/*
 * App
 *
 */
class App extends EventDispatcher {
	
	constructor () {
		super();
		
		//params
		this._mock = false;
		this._config = null;
		this._container = null;
		this._extraPaths = {};
		this._baseURL = PathUtils.getLocation(); // URL of main javascript file
		this._projURL = PathUtils.getBase();
		this._fullProjURL = this._projURL; // The lang is concatenated in setup
	}
	
	// Getters And Setters
	//----------------------------------------------------------
	get config () {return this._config || {};}
	get mock () {return this._mock;}
	get lang () {return LanguageController;}
	get language () {return this._language;}
	get baseURL () {return this._baseURL;}
	get projURL () {return this._projURL;}
	get fullProjURL () {return this._fullProjURL;}

	get container () {return this._container;}
	set container (value) {this._container = value;}
	
	get paths () {
		return {
			...this._extraPaths,
			lang: this.language,
			baseURL: this.baseURL,
			projURL: this.projURL,
			fullProjURL: this.fullProjURL
		}
	}
	set paths (value) {
		this._extraPaths = value;
	}

	// Public
	//-------------------------------------------------------
	setup (config = {}) {
		if(config.setup) {
			//setup multilanguage
			let langData = this._setupLanguage(config.setup.multilingual);
			if (langData) {
				this._language = langData.current || langData.default;
				this._languageList = langData.list;
				this._fullProjURL = (this._projURL + langData.current + "/").replace(/\/+/, "/");
			}
		}
		
		//replace paths config file
		this._config = JSONUtils.replace(config, this.paths);
	}

	changeLanguage (lang) {
		let link = PathUtils.getProjectPaths(location.href, app.baseURL);
		let depths = link.split("/");

		//replace or add langauge path
		if(this._languageList && depths[0]) {
			if(this._languageList.indexOf(depths[0]) != -1) {
				depths[0] = lang;
			}
			else {
				depths.unshift(lang);
			}
		}
		else {
			depths[0] = lang;
		}
		
		//replace or add langauge path
		self.location.href = this._projURL + depths.join("/");
	}

	startMultiLanguage (data) {
		this.lang.addDictionary(data);
	}

	isMobileSize () {
		return window.innerWidth <= 767;
	}

	// Private
	//-------------------------------------------------------
	_setupLanguage (data = {}) {
		if(data.list && data.list.length) {
			let def = data.default || data.list[0];
			let pos = data.position || 0;
			let url = PathUtils.getProjectPaths(location.href, app.baseURL);
			let language = url.split("/")[0];
			
			//validate founded language
			if (data.list.indexOf(language) == -1) {
				language = "";
			}
			return {
				list: data.list,
				default: def,
				current: language
			}
		}
		else if(data.default) {
			return {
				list: [],
				default: data.default,
				current: ""
			}
		}
	}
}

// make the instance gloabal
window.app = new App();