/*
 * Particle
 * ~fr/animations/Vector.js
 */
export default class Vector {

	constructor (x = 0, y = 0) {
		this._x = x;
		this._y = y;
	}
	
	get x () {return this._x}
	set x (val) {this._x = val}

	get y () {return this._y}
	set y (val) {this._y = val}
	
	get angle () {return Math.atan2(this._y, this._x)}
	set angle (value) {
		let length = this.length;
		this._x = Math.cos(value) * length;
		this._y = Math.sin(value) * length;
	}
	
	get length () {return Math.sqrt(this.x * this.x + this.y * this.y)}
	set length (value) {
		let angle = this.angle;
		this._x = Math.cos(angle) * value;
		this._y = Math.sin(angle) * value;
	}

	add (vector) {
		this._x += vector.x;
		this._y += vector.y;
	}
}