// framework
import ArrayUtils from '~fr/utils/ArrayUtils.js';
import Detections from '~fr/utils/Detections.js';

/*
 * MouseEvents
 *
 */
class MouseEvents {

	constructor () {
		this._events = this._getEvents();
	}

	// Getters
	//-------------------------------------------------------
	get START () {return this._events.start;}
	get END () {return this._events.end;}
	get MOVE () {return this._events.move;}

	// Public
	//-------------------------------------------------------
	getTouchData (event) {
		if(event.touches) {
			return event.touches[0];
		}
		else {
			return event;
		}
	}

	// Private
	//-------------------------------------------------------
	_getEvents () {
		if(Detections.desktop) {
			return {
				start: "mousedown",
				end: "mouseup",
				move: "mousemove"
			}		
		}
		else {
			return {
				start: "touchstart",
				end: "touchend",
				move: "touchmove"
			}
		}
	}
}

export default new MouseEvents();