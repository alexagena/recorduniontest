// framework
import ArrayUtils from '~fr/utils/ArrayUtils.js';

/*
 * EventDispatcher
 *
 */
export default class EventDispatcher {

	constructor () {
		this._events = {};
	}

	// Public
	//----------------------------------------------------------
	on(p_event, p_handler, p_useCapture = false) {
		if(!this._events) {
			this._events = {};
		}
		if(!this._events[p_event]) {
			this._events[p_event] = [];
		}
		if(!ArrayUtils.contain(this._events[p_event], p_handler)) {
			this._events[p_event].unshift(p_handler);
		}
	}
	
	off(p_event=null, p_handler=null, p_useCapture=false) {
		if(!this._events) {
			this._events = {};
			return;
		}
		if(p_event != undefined && this._events[p_event]) {
			var events = this._events[p_event];
			if(!p_handler) {
				this._events[p_event].length = 0;
			}
			else {
				let i;
				while((i = events.indexOf(p_handler)) >= 0) {
					events.splice(i, 1)
				}
				this._events[p_event] = events;
			}
		}
		else {
			this._events = events;
		}
	}
	
	trigger (evt, data = null, target = null) {
		if(Array.isArray(evt)) {
			for(var i = 0; i < evt.length; i++) {
				this.trigger(evt, data);
			}
			return
		}
		if(!this._events) {
			this._events = {};
		}
		var events = this._events[evt];
		if(!events || events.length == 0) {
			return;
		}
		if(!target) {
			target = this;
		}
		var e = {
			type: evt,
			target: target,
			currentTarget: this
		}
		if(typeof(data) == 'object') {
			for(var k in data) {
				var v = [k];
				if(e[k]) {
					e[k] = v;
				}
			}
		}
		var i = events.length;
		while(i-- > 0) {
			if(events[i]) {
				events[i](e, data);
			}
		}
	}
	
	hasEvent () {
		if(!this._events) {
			this._events = {};
			return;
		}
		for(var i = 0; i < this._events.length; i++) {
			var event = this._events[i];
			if(event == p_event) {
				if(this._events[event].indexOf(p_handler) > -1) {
					return true;
				}
			}
		}
	}
}