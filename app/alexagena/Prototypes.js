HTMLElement.prototype.addClass = function (value = '') {
	let className = this.className.replace(/\s+/g, ' '); 
	let classList = className.split(' ');

	for(let cssClass of classList) {
		if(cssClass == value) {
			return;
			break;
		}
	}

	classList.push(value);
	this.className = classList.join(' '); 
};

HTMLElement.prototype.removeClass = function (value = '') {
	let className = this.className.replace(/\s+/g, ' '); 
	let classList = className.split(' ');
	let i = 0;

	for(let cssClass of classList) {
		if(cssClass == value) {
			classList.splice(i, 1);
		}
		i++;
	}
	this.className = classList.join(' ');
}

HTMLElement.prototype.hasClass = function (value = '') {
	let className = this.className.replace(/\s+/g, ' '); 
	let classList = className.split(' ');
	return classList.indexOf(value) != -1;
}

HTMLElement.prototype.parents = function(selector) {
	var elements = [];
	var elem = this;
	var ishaveselector = selector !== undefined;
 
	while ((elem = elem.parentElement) !== null) {
		if (elem.nodeType !== Node.ELEMENT_NODE) {
			continue;
		}
		if (!ishaveselector || elem.matches(selector)) {
			elements.push(elem);
		}
	}
 
	return elements;
};