;(function(window, undefined) {
	"use strict";

	function extend() {
		for(var i=1; i < arguments.length; i++) {
			for(var key in arguments[i]) {
				if(arguments[i].hasOwnProperty(key)) {
					arguments[0][key] = arguments[i][key];
				}
			}
		}
		return arguments[0];
	}

	var pluginName = "tinyscrollbar",
		defaults = {
			axis: 'y',
			ease: Quart.easeOut,
			use3D: true,
			wheel: true,
			wheelSpeed: 40,
			wheelInertia: 0.6,
			wheelLock: true,
			touchLock: true,
			trackSize: false,
			thumbSize: false,
			thumbSizeMin: 20
		};

	function Plugin($container, options) {
		/**
		 * The options of the carousel extend with the defaults.
		 *
		 * @property options
		 * @type Object
		 * @default defaults
		 */
		this.options = extend({}, defaults, options);

		/**
		 * @property _defaults
		 * @type Object
		 * @private
		 * @default defaults
		 */
		this._defaults = defaults;

		/**
		 * @property _name
		 * @type String
		 * @private
		 * @final
		 * @default 'tinyscrollbar'
		 */
		this._name = pluginName;

		var self = this
		,   $body = document.querySelectorAll("body")[0]
		,   $viewport = $container.querySelectorAll(".viewport")[0]
		,   $overview = $container.querySelectorAll(".overview")[0]
		,   $scrollbar = $container.querySelectorAll(".scrollbar")[0]
		,   $track = $scrollbar.querySelectorAll(".track")[0]
		,   $thumb = $scrollbar.querySelectorAll(".thumb")[0]

		,   mousePosition = 0
		,   isHorizontal = this.options.axis === 'x'
		,   hasTouchEvents = ("ontouchstart" in document.documentElement)
		,   wheelEvent = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
							 document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
							 "DOMMouseScroll" // let's assume that remaining browsers are older Firefox

		,   sizeLabel = isHorizontal ? "width" : "height"
		,   posiLabel = isHorizontal ? "left" : "top"
		,   moveEvent = document.createEvent("HTMLEvents")
		;

		moveEvent.initEvent("move", true, true);

		/**
		 * The position of the content relative to the viewport.
		 *
		 * @property contentPosition
		 * @type Number
		 * @default 0
		 */
		this.contentPosition = 0;
		this.currentPosition = 0;

		/**
		 * The height or width of the viewport.
		 *
		 * @property viewportSize
		 * @type Number
		 * @default 0
		 */
		this.viewportSize = 0;

		/**
		 * The height or width of the content.
		 *
		 * @property contentSize
		 * @type Number
		 * @default 0
		 */
		this.contentSize = 0;

		/**
		 * The ratio of the content size relative to the viewport size.
		 *
		 * @property contentRatio
		 * @type Number
		 * @default 0
		 */
		this.contentRatio = 0;

		/**
		 * The height or width of the content.
		 *
		 * @property trackSize
		 * @type Number
		 * @default 0
		 */
		this.trackSize = 0;

		/**
		 * The size of the track relative to the size of the content.
		 *
		 * @property trackRatio
		 * @type Number
		 * @default 0
		 */
		this.trackRatio = 0;

		/**
		 * The height or width of the thumb.
		 *
		 * @property thumbSize
		 * @type Number
		 * @default 0
		 */
		this.thumbSize = 0;

		/**
		 * The position of the thumb relative to the track.
		 *
		 * @property thumbPosition
		 * @type Number
		 * @default 0
		 */
		this.thumbPosition = 0;

		/**
		 * Will be true if there is content to scroll.
		 *
		 * @property hasContentToScroll
		 * @type Boolean
		 * @default false
		 */
		this.hasContentToSroll = false;

		/**
		 * @method _initialize
		 * @private
		 */
		function _initialize() {
			self.update();
			_setEvents();
			return self;
		}

		/**
		 * You can use the update method to adjust the scrollbar to new content or to move the scrollbar to a certain point.
		 *
		 * @method update
		 * @chainable
		 * @param {Number|String} [scrollTo] Number in pixels or the values "relative" or "bottom". If you dont specify a parameter it will default to top
		 */
		this.update = function(scrollTo) {
			var sizeLabelCap = sizeLabel.charAt(0).toUpperCase() + sizeLabel.slice(1).toLowerCase();
			var scrcls = $scrollbar.className;

			this.viewportSize = $viewport['offset'+ sizeLabelCap];
			this.contentSize = $overview['scroll'+ sizeLabelCap];
			this.contentRatio = this.viewportSize / this.contentSize;
			this.trackSize = this.options.trackSize || this.viewportSize;
			this.thumbSize = Math.min(this.trackSize, Math.max(this.options.thumbSizeMin, (this.options.thumbSize || (this.trackSize * this.contentRatio))));
			this.trackRatio = (this.contentSize - this.viewportSize) / (this.trackSize - this.thumbSize);
			this.hasContentToSroll = this.contentRatio < 1;
			$scrollbar.className = this.hasContentToSroll ? scrcls.replace(/disable/g, "") : scrcls.replace(/ disable/g, "") + " disable";

			switch (scrollTo) {
				case "bottom":
					this.contentPosition = Math.max(this.contentSize - this.viewportSize, 0);
					break;
				case "relative":
					this.contentPosition = Math.min(Math.max(this.contentSize - this.viewportSize, 0), Math.max(0, this.contentPosition));
					break;
				default:
					this.contentPosition = parseInt(scrollTo, 10) || 0;
			}
			this.thumbPosition = self.contentPosition / self.trackRatio;
			self.currentPosition = self.contentPosition;
			_setCss();
			return self;
		};

		/**
		 * @method _setCss
		 * @private
		 */
		function _setCss() {
			self.thumbSize = self.thumbSize || 0;
			self.thumbPosition = self.thumbPosition || 0;
			if(self.options.use3D) {
				if(posiLabel == "top") {
					TweenMax.set($thumb, {top: self.thumbPosition, force3D: true, ease: self.options.ease});
					TweenMax.set($overview, {y: -self.currentPosition, force3D: true, ease: self.options.ease});
					TweenMax.set($scrollbar, {height: self.trackSize, force3D: true, ease: self.options.ease});
					TweenMax.set($track, {height: self.trackSize, force3D: true, ease: self.options.ease});
					TweenMax.set($thumb, {height: self.thumbSize, force3D: true, ease: self.options.ease});
				}
				else {
					TweenMax.set($thumb, {left: self.thumbPosition, ease: self.options.ease});
					TweenMax.set($overview, {x: -self.currentPosition, ease: self.options.ease});
					TweenMax.set($scrollbar, {width: self.trackSize, ease: self.options.ease});
					TweenMax.set($track, {width: self.trackSize, ease: self.options.ease});
					TweenMax.set($thumb, {width: self.thumbSize, ease: self.options.ease});
				}
			}
			else {
				if(posiLabel == "top") {
					TweenMax.set($thumb, {top: self.thumbPosition});
					TweenMax.set($overview, {top: -self.currentPosition});
					TweenMax.set($scrollbar, {height: self.trackSize});
					TweenMax.set($track, {height: self.trackSize});
					TweenMax.set($thumb, {height: self.thumbSize});
				}
				else {
					TweenMax.set($thumb, {left: self.thumbPosition});
					TweenMax.set($overview, {left: -self.currentPosition});
					TweenMax.set($scrollbar, {width: self.trackSize});
					TweenMax.set($track, {width: self.trackSize});
					TweenMax.set($thumb, {width: self.thumbSize});
				}
			}
		}

		/**
		 * @method _setEvents
		 * @private
		 */
		function _setEvents() {
			if(hasTouchEvents) {
				$viewport.ontouchstart = function(event) {
					if(1 === event.touches.length) {
						_start(event.touches[0]);
						event.stopPropagation();
					}
				};
			}
			$thumb.onmousedown = function(event) {
				event.stopPropagation();
				_start(event);
			};

			$track.onmousedown = function(event) {
				_start(event, true);
			};

			window.addEventListener("resize", function() {
			   self.update("relative");
			}, true);

			if(self.options.wheel && window.addEventListener) {
				$container.addEventListener(wheelEvent, _wheel, false );
			}
			else if(self.options.wheel) {
				$container.onmousewheel = _wheel;
			}
		}

		/**
		 * @method _isAtBegin
		 * @private
		 */
		function _isAtBegin() {
			return self.contentPosition > 0;
		}

		/**
		 * @method _isAtEnd
		 * @private
		 */
		function _isAtEnd() {
			return self.contentPosition <= (self.contentSize - self.viewportSize) - 5;
		}

		/**
		 * @method _start
		 * @private
		 */
		function _start(event, gotoMouse) {
			if(self.hasContentToSroll) {
				var posiLabelCap = posiLabel.charAt(0).toUpperCase() + posiLabel.slice(1).toLowerCase();
				mousePosition = gotoMouse ? $thumb.getBoundingClientRect()[posiLabel] : (isHorizontal ? event.clientX : event.clientY);

				$body.className += " noSelect";
				$scrollbar.setAttribute("data-scrolling", "true");

				if(hasTouchEvents) {
					document.ontouchmove = function(event) {
						if(self.options.touchLock || _isAtBegin() && _isAtEnd()) {
							event.preventDefault();
						}
						event.touches[0][pluginName + "Touch"] = 1;
						_drag(event.touches[0]);
					};
					document.ontouchend = _end;
				}
				document.onmousemove = _drag;
				document.onmouseup = $thumb.onmouseup = _end;
				_drag(event);
			}
		}

		/**
		 * @method _wheel
		 * @private
		 */
		function _wheel(event) {
			if(self.hasContentToSroll) {
				var evntObj = event || window.event
				,   wheelSpeedDelta = _normalizeDelta(evntObj)//-(evntObj.deltaY || evntObj.detail || (-1 / 3 * evntObj.wheelDelta)) / 40
				,   multiply = (evntObj.deltaMode === 1) ? self.options.wheelSpeed : 1;

				self.contentPosition -= wheelSpeedDelta * self.options.wheelSpeed;
				self.contentPosition = Math.min((self.contentSize - self.viewportSize), Math.max(0, self.contentPosition));
				self.thumbPosition = self.contentPosition / self.trackRatio;

				//update scroll position
				TweenMax.to(self, self.options.wheelInertia, {
					currentPosition: self.contentPosition,
					ease: self.options.ease,
					onUpdate: _setScrollPosition.bind(this)
				});
				
				//update thumb position
				if(self.options.use3D) {
					if(posiLabel == "top") {
						TweenMax.to($thumb, self.options.wheelInertia, {top: self.thumbPosition, force3D: true, ease: self.options.ease});
					}
					else {
						TweenMax.to($thumb, self.options.wheelInertia, {left: self.thumbPosition, force3D: true, ease: self.options.ease});
					}
				}
				else {
					if(posiLabel == "top") {
						TweenMax.to($thumb, self.options.wheelInertia, {top: self.thumbPosition, ease: self.options.ease});
					}
					else {
						TweenMax.to($thumb, self.options.wheelInertia, {left: self.thumbPosition, ease: self.options.ease});
					}
				}
				
				if(self.options.wheelLock || _isAtBegin() && _isAtEnd()) {
					event.stopPropagation();
					evntObj.preventDefault();
				}
			}
		}

		function _normalizeDelta (e) {
			var o = e,
				d = o.detail,
				w = o.wheelDelta || (e.deltaY < 0 ? 180 : -180),
				n = 225,
				n1 = n-1;

			// Normalize delta
			d = d ? w && (f = w/d) ? d/f : -d/1.35 : w/120;
			// Quadratic scale if |d| > 1
			d = d < 1 ? d < -1 ? (-Math.pow(d, 2) - n1) / n : d : (Math.pow(d, 2) + n1) / n;
			// Delta *should* not be greater than 2...
			return Math.min(Math.max(d / 2, -1), 1) * 3;
		}

		/**
		 * @method _drag
		 * @private
		 */
		function _drag(event) {
			if(self.hasContentToSroll)
			{
				var mousePositionNew = isHorizontal ? event.clientX : event.clientY
				,   thumbPositionDelta = event[pluginName + "Touch"] ? (mousePosition - mousePositionNew) : (mousePositionNew - mousePosition)
				,   thumbPositionNew = Math.min(self.trackSize - self.thumbSize, Math.max(0, self.thumbPosition + thumbPositionDelta))
				;
				self.contentPosition = thumbPositionNew * self.trackRatio;

				//update scroll position
				TweenMax.to(self, self.options.wheelInertia, {
					currentPosition: self.contentPosition,
					ease: self.options.ease,
					onUpdate: _setScrollPosition.bind(this)
				});

				//update scroll position
				if(self.options.use3D) {
					if(posiLabel == "top") {
						TweenMax.to($thumb, self.options.wheelInertia, {top: thumbPositionNew, force3D: true, ease: self.options.ease});
					}
					else {
						TweenMax.to($thumb, self.options.wheelInertia, {left: thumbPositionNew, force3D: true, ease: self.options.ease});
					}
				}
				else {
					if(posiLabel == "top") {
						TweenMax.to($thumb, self.options.wheelInertia, {top: thumbPositionNew, ease: self.options.ease});
					}
					else {
						TweenMax.to($thumb, self.options.wheelInertia, {left: thumbPositionNew, ease: self.options.ease});
					}
				}
			}
		}

		function _setScrollPosition () {
			if(self.options.use3D) {
				if(posiLabel == "top") {
					TweenMax.set($overview, {y: -self.currentPosition, force3D: true, ease: self.options.ease});
				}
				else {
					TweenMax.set($overview, {x: -self.currentPosition, force3D: true, ease: self.options.ease});
				}
			}
			else {
				if(posiLabel == "top") {
					TweenMax.set($overview, {top: -self.currentPosition, ease: self.options.ease});
				}
				else {
					TweenMax.set($overview, {left: -self.currentPosition, ease: self.options.ease});
				}
			}
			$container.dispatchEvent(moveEvent);
		}

		/**
		 * @method _end
		 * @private
		 */
		function _end() {
			self.thumbPosition = parseInt($thumb.style[posiLabel], 10) || 0;
			$body.className = $body.className.replace(" noSelect", "");
			document.onmousemove = document.onmouseup = null;
			$thumb.onmouseup = null;
			$track.onmouseup = null;
			$scrollbar.removeAttribute("data-scrolling");
			document.ontouchmove = document.ontouchend = null;
		}
		
		return _initialize();
	}
	
	/**
	* @class window.tinyscrollbar
	* @constructor
	* @param {Object} [$container] Element to attach scrollbar to.
	* @param {Object} options
		@param {String} [options.axis='y'] Vertical or horizontal scroller? ( x || y ).
		@param {Boolean} [options.wheel=true] Enable or disable the mousewheel.
		@param {Boolean} [options.wheelSpeed=40] How many pixels must the mousewheel scroll at a time.
		@param {Boolean} [options.wheelLock=true] Lock default window wheel scrolling when there is no more content to scroll.
		@param {Number} [options.touchLock=true] Lock default window touch scrolling when there is no more content to scroll.
		@param {Boolean|Number} [options.trackSize=false] Set the size of the scrollbar to auto(false) or a fixed number.
		@param {Boolean|Number} [options.thumbSize=false] Set the size of the thumb to auto(false) or a fixed number
		@param {Boolean} [options.thumbSizeMin=20] Minimum thumb size.
	*/
	var tinyscrollbar = function($container, options) {
		return new Plugin($container, options);
	};

	if(typeof define == 'function' && define.amd) {
		define(function(){ return tinyscrollbar; });
	}
	else if(typeof module === 'object' && module.exports) {
		module.exports = tinyscrollbar;
	}
	else {
		window.tinyscrollbar = tinyscrollbar;
	}
	
})(window);
