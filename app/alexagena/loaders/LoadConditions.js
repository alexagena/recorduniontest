//framework
import Detections from '~fr/utils/Detections.js';
import ResolveConditions from '~fr/utils/ResolveConditions.js';

/*
 * LoadConditions
 *
 */
class LoadConditions {
	
	// Public
	//----------------------------------------------------------
	test (expression) {
		return ResolveConditions.test(expression, (condition)=>{
			let params = condition.split(':');
			let method = "_" + params[0];
			let rest = params.slice(1, params.length);
			if(this[method]) {
				return this[method](...rest);
			}
			return false;
		});
	}

	// Private
	//-------------------------------------------------------
	_isDesktop () {
		return Detections.desktop;
	}

	_isMobile () {
		return Detections.mobile;
	}

	_retina (value) {
		let retina = window.devicePixelRatio;
		let compare = value.match(/[^(0-9)]+/);

		value = parseFloat(value.replace(compare, ""));
		compare = compare ? compare[0].trim() : "";

		switch(compare) {
			case ">":
				return retina > value;
				break;
			case ">=":
				return retina >= value;
				break;
			case "<":
				return retina < value;
				break;
			case "<=":
				return retina <= value;
				break;
			default:
				return retina == value;
		}
	}
}

export default new LoadConditions();