//third party
import '~fr/third-party/preload-js';
//framework
import JSONUtils from '~fr/utils/JSONUtils.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';
//relative
import LoadConditions from './LoadConditions.js';

/*
 * GroupLoader
 * 
 */
export default class GroupLoader extends EventDispatcher {

	constructor (id) {
		//call parent
		super();

		//bind
		this._onFileLoaded = this._onFileLoaded.bind(this);
		this._onLoadComplete = this._onLoadComplete.bind(this);
		this._onLoadProgress = this._onLoadProgress.bind(this);

		//params
		this._id = id; 
		this._isLoading = false;
		this._loadedFiles = [];
		this._currentPhase = {};
		this._phases = [
			GroupLoader.PHASE_INITIAL, GroupLoader.PHASE_PRELOADER,
			GroupLoader.PHASE_BACKGROUND, GroupLoader.PHASE_AVOIDED
		];
		this._loadingPhases = this._createPhases(this._phases);
	}
	
	// Getters And Setters
	//----------------------------------------------------------
	static get PROGRESS () {return 'groupLoaderProgress';}
	static get COMPLETE () {return 'groupLoaderComplete';}
	static get FILE_LOADED () {return 'groupLoaderFileLoaded';}
	static get PHASE_INITIAL () {return 'initial';}
	static get PHASE_PRELOADER () {return 'preloader';}
	static get PHASE_BACKGROUND () {return 'background';}
	static get PHASE_AVOIDED () {return 'avoid';}

	get id () {return this._id;}
	get queue () {return this._queue;}
	get loadedFiles () {return this._loadedFiles;}
	get numItems () {
		if(this._currentPhase.queue) {
			return this._currentPhase.queue._numItems;
		}
		return 0;
	}
	get progress () {
		if(this._currentPhase.queue) {
			if(this._currentPhase.queue.loaded || this.numItems == 0) {
				return 1;
			}
			return this._currentPhase.queue.progress;
		}
		return 0;
	}

	// Public
	//----------------------------------------------------------
	addFiles (items, phase) {
		items = [].concat(items);
		for(var i = 0; i < items.length; i++) {
			this._screeningFile(items[i], phase);
		}
		if(this._isLoading) {
			this.load(this._currentPhase.name);
		}
	}
	
	addContent (json, contentPhase) {
		//filter json
		var itemsToLoad = JSONUtils.filterObject(json, 'src', null, null, true);
		//add to preloader
		if(itemsToLoad.length > 0) {
			this.addFiles(itemsToLoad, contentPhase);
		}
	}

	selectPhase (phase) {
		this._currentPhase.name = phase;
		this._currentPhase.list = this._loadingPhases[phase].list;
		this._currentPhase.queue = this._loadingPhases[phase].queue;
	}

	gotoNextPhase () {
		let nextPhase;
		for(let i = 0; i < this._phases.length; i++) {
			if(this._phases[i] == this._currentPhase.name) {
				nextPhase = this._phases[i + 1];
			}
		}
		if(nextPhase) {
			this.selectPhase(nextPhase);
		}
	}

	load (phase = this._currentPhase.name) {
		this._isLoading = true;
		this.selectPhase(phase);
		//load assets
		if(!this._currentPhase.queue.loaded) {
			this._currentPhase.queue.load();
		}
		else {
			this._onLoadComplete();
		}
	}
	
	pause () {
		this._isLoading = false;
	}

	kill () {

	}
	
	// Private
	//----------------------------------------------------------
	_createPhases (phases) {
		let loadingPhases = {};
		for(let i = 0; i < phases.length; i++) {
			let phase = {
				name: phases[i],
				list: [],
				queue: new createjs.LoadQueue()
			};
			phase.queue.setMaxConnections(5);
			phase.queue.on('fileload', this._onFileLoaded);
			phase.queue.on('complete', this._onLoadComplete);
			phase.queue.on('progress', this._onLoadProgress);
			//save current item
			loadingPhases[phases[i]] = phase;
		}
		return loadingPhases;
	}

	_screeningFile (item, phase) {
		//copy content to src
		if(item.content) {
			item.src = item.content;
		}
		//load
		if(!item.when || LoadConditions.test(item.when)) {
			phase = this._getItemPhase(item, phase);
			phase.queue.loadFile(item, false);
		}
	}

	_getItemPhase (item, phase) {
		//select wich phase file will be downloaded
		if(item.loadPhase) {
			switch (item.loadPhase) {
				case GroupLoader.PHASE_INITIAL:
					phase = this._loadingPhases[GroupLoader.PHASE_INITIAL];
					break;
				case GroupLoader.PHASE_PRELOADER:
					phase = this._loadingPhases[GroupLoader.PHASE_PRELOADER];
					break;
				case GroupLoader.PHASE_BACKGROUND:
					phase = this._loadingPhases[GroupLoader.PHASE_BACKGROUND];
					break;
				case GroupLoader.PHASE_AVOIDED:
					phase = this._loadingPhases[GroupLoader.PHASE_AVOIDED];
					break;
			}
		}
		else if(phase) {
			phase = this._loadingPhases[phase];
		}
		//if didnt found any match
		if(!phase) {
			//if is a content file, is important to set src with content value
			if(item.content) {
				phase = this._loadingPhases[GroupLoader.PHASE_INITIAL];
			}
			//default value
			else {
				phase = this._loadingPhases[GroupLoader.PHASE_PRELOADER];
			}
		}
		return phase;
	}
	
	_onFileLoaded (e) {
		//update list
		this._loadedFiles.push(e.item);

		//fix depending on file extension
		switch (e.item.ext) {
			case 'json':
				e.result = JSONUtils.replace(e.result, app.paths);
				this.addContent(e.result, e.item.contentLoadPhase);
				break;
			case 'js':
				break;
		}
		//replace result
		if(e.item.content) {
			e.item.content = e.result;
		}
		else {
			e.item.result = e.result;
		}
		//trigger event
		this.trigger(GroupLoader.FILE_LOADED, e);
	}

	_onLoadComplete (e) {
		this._isLoading = false;
		this.trigger(GroupLoader.COMPLETE, this._currentPhase);
	}

	_onLoadProgress (e) {
		if(this._currentPhase.queue) {
			this.trigger(GroupLoader.PROGRESS, this._currentPhase.name);
		}
	}
}