//third party
import '~fr/third-party/preload-js';
//framework
import GroupLoader from '~fr/loaders/GroupLoader.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * AssetLoader
 *
 */
export default class AssetLoader extends EventDispatcher {
	
	constructor () {
		//call parent
		super();

		//params
		this._count = 0;
		this._groups = [];
		
		//bind
		this._onGroupLoading = this._onGroupLoading.bind(this);
	}
	
	static get PROGRESS () {return "AssetLoaderProgress";}

	// Public
	//----------------------------------------------------------
	getGroup (id) {
		if(!this._groups[id]) {
			this._groups[id] = new GroupLoader(id);
			this._groups[id].on(GroupLoader.PROGRESS, this._onGroupLoading);
		}
		return this._groups[id];
	}

	// Private
	//-------------------------------------------------------
	_onGroupLoading (e, phaseName) {
		let total = 0;
		let progress = 0;
		for(let key in this._groups) {
			let group = this._groups[key];
			total += group.numItems;
			progress += group.progress * group.numItems;
		}
		this.trigger(AssetLoader.PROGRESS, [phaseName, progress / total]);
	}
}