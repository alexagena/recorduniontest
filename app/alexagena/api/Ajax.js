//third party
import 'fetch-polyfill';
//framework
import JSONUtils from '~fr/utils/JSONUtils.js';
import StringUtils from '~fr/utils/StringUtils.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * Ajax
 * 
 */
class Ajax extends EventDispatcher {

	// Public
	//-------------------------------------------------------
	fetch (url, opts = {}) {
		//defaults
		let options = {
			method: "GET",
			headers: {
				"Content-Type": this._getContentType(opts.dataType),
				"X-Custom-Header": "ProcessThisImmediately"
			},
			mode: "cors",
			cache: "default",
			...opts
		}
		//fix body
		if(typeof(options.body) == "object") {
			options.body = JSON.stringify(options.body);
		}
		//return fetch
		return fetch(url, options);
	}

	jsonp (url) {
		var name = "_jsonp_" + StringUtils.createKey();
		if (url.match(/\?/))
			url += "&callback=" + name;
		else
			url += "?callback=" + name;

		//create script
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;

		//setup handler
		var promise = new Promise((resolve, reject)=>{
			window[name] = function(data) {
				resolve(data);
				document.getElementsByTagName('head')[0].removeChild(script);
				script = null;
				delete window[name];
			};
		});
		
		//load JSON
		document.getElementsByTagName('head')[0].appendChild(script);
		return promise;
	}
	
	// Private
	//-------------------------------------------------------
	_getContentType (contentType) {
		switch(contentType) {
			case "json":
				return "application/json";
				break;
			case "javascript":
				return "application/javascript";
				break;
			case "xml":
				return "text/xml";
				break;
			case "html":
				return "text/html";
				break;
		}
		return "text/plain";
	}
}

export default new Ajax();