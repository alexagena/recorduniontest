//framework
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * GoogleMapsAPI
 * 
 */
class GoogleMapsAPI extends EventDispatcher {

	constructor () {
		//call parent
		super();
		
		//params
		this._apiKey = "";
		this._apiURL = "https://maps.googleapis.com/maps/api/js";
		this._loading = false;
		this._loaded = false;
		this._maps = [];
		this._markers = [];
	}

	get LOADED () {return "GoogleMapsAPILoaded"}
	
	get apiKey () {return this._apiKey}
	get loaded () {return this._loaded}
	get loading () {return this._loading}

	get apiURL () {return this._apiURL}
	set apiURL (value) {this._apiURL = value}

	// Public
	//-------------------------------------------------------
	load (apiKey) {
		if(apiKey && !this._loading && !this._loaded) {
			this._loading = true;
			this._apiKey = apiKey;

			//load api script
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = this._apiURL + "?key=" + this._apiKey;
			document.getElementsByTagName('head')[0].appendChild(script);

			//wait finish loading
			let timer = setInterval(()=>{
				if(typeof(google) == "object" && google.maps) {
					this._loaded = true;
					this._loading = false;
					this.trigger(this.LOADED);
					clearInterval(timer);
				}
			}, 200);
		}
	}

	createMap (element, data) {
		if(!this._getMapByElement(element)) {
			let item = {
				element: element,
				map: new google.maps.Map(element, data)
			} 
			this._maps.push(item);
			return item;
		}
	}

	addMarker (element, position, title) {
		let item = this._getMapByElement(element);
		if (item.map) {
			let params = {
				map: item.map,
				title: title,
				position: position
			}
			let marker = new google.maps.Marker(params);
			this._markers.push({marker, params});
		}
	}

	// Privte
	//-------------------------------------------------------
	_getMapByElement (element) {
		for(let i = 0; i < this._maps.length; i++) {
			if(this._maps[i].element == element) {
				return this._maps[i];
				break;
			}
		}
		return false;
	}
}

export default new GoogleMapsAPI();