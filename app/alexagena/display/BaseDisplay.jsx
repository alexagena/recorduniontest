// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';

/*
 * BaseDisplay
 * ~fr/display/BaseDisplay.jsx
 */
export default class BaseDisplay extends React.Component {

	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this.showed = false;
		this.stateReady = false;
		this.buildReady = false;
		this.state = {
			element: props.element || "",
			className: props.className || ""
		};
		
		//bind methods
		this.showComplete = this.showComplete.bind(this);
		this.hideComplete = this.hideComplete.bind(this);
	}
	
	static get propTypes () {
		return {
			element: PropTypes.string,
			className: PropTypes.string
		}
	}

	static get defaultProps () {
		return {
			element: "div",
			className: ""
		}
	}

	get wrapper () {
		return this.refs.wrapper;
	}
	
	get wrapWidth () {
		if(this.refs.wrapper) {
			return this.refs.wrapper.offsetWidth;
		} else {
			return 0;
		}
	}
	
	get wrapHeight () {
		if(this.refs.wrapper) {
			return this.refs.wrapper.offsetHeight;
		} else {
			return 0;
		}
	}

	// React
	//-------------------------------------------------------
	componentWillMount () {
		this.stateReady = true;
	}

	componentDidMount () {
		this.buildReady = true;
		if(!this.refs.wrapper && !this.refs.input) {
			throw new Error("wrapper reference is required! " + this.props.className);
		}
	}
	
	shouldComponentUpdate(nextProps, nextState) {
		let compare = shallowCompare(this, nextProps, nextState);
		return compare;
	}
	
	// Public
	//----------------------------------------------------------
	prepareToShow () {
		
	}
	
	show (p_factor = 1, p_delay = 0) {
		if(this.refs.wrapper) {
			this.refs.wrapper.style.display = '';
		}
		this.showComplete();
	}

	showComplete () {
		setTimeout(()=>{
			this.showed = true;
			this.triggerEvent('onShowComplete');
		}, 10);
	}

	prepareToHide () {
		
	}
	
	hide (p_factor = 1, p_delay = 0) {
		if(this.refs.wrapper) {
			this.refs.wrapper.style.display = 'none';
		}
		this.hideComplete();
	}
	
	hideComplete () {
		setTimeout(()=>{
			this.showed = false;
			this.triggerEvent('onHideComplete');
		}, 10);
	}
	
	// Destroy
	//----------------------------
	destroy () {
		
	}

	// Events
	//----------------------------
	triggerEvent (name, event = {}, ...data) {
		let method;
		//when only one parameter, avoid rest variable 
		if(typeof(data) == "object" && data.length == 1) {
			data = data[0];
		}
		//try trigger function passed by props
		if(typeof(this.props[name]) == 'function') {
			method = this.props[name];
		}
		//when method passed by direct param
		else if(typeof(this[name]) == 'function') {
			method = this[name];
		}
		//if founded the method
		if(method) {
			if(event.constructor.name.indexOf("Synthetic") != -1) {
				event = {
					name: name,
					target: this, 
					reactEvent: event
				}
			}
			else {
				event.target = this;
			}
			method(event, data);
		}
	}
	
	// Manage classnames
	//----------------------------
	addClass (value = '', debug = false) {
		let className = this.state.className.replace(/\s+/g, ' ').trim(); 
		let classList = (className && className.split(' ')) || [];
		
		for(let cssClass of classList) {
			if(cssClass == value) {
				return;
				break;
			}
		}
		classList.push(value);
		className = classList.join(' ');
		this.state.className = className;

		if(this.stateReady) {
			if(this.refs.wrapper) {
				this.refs.wrapper.className = className;
			}
			else if(this.refs.input) {
				this.refs.input.className = className;
			}
		}
		return className;
	}

	removeClass (value = '', debug = false) {
		let className = this.state.className.replace(/\s+/g, ' ').trim(); 
		let classList = (className && className.split(' ')) || [];
		let i = 0;

		for(let cssClass of classList) {
			if(cssClass == value) {
				classList.splice(i, 1);
			}
			i++;
		}
		className = classList.join(' ');
		this.state.className = className;
		
		if(this.stateReady) {
			if(this.refs.wrapper) {
				this.refs.wrapper.className = className;
			}
			else if(this.refs.input) {
				this.refs.input.className = className;
			}
		}
		return className;
	}
	
	hasClass (value) {
		let className = this.state.className.replace(/\s+/g, ' ').trim(); 
		let classList = (className && className.split(' ')) || [];
		return classList.indexOf(value) != -1;
	}
}