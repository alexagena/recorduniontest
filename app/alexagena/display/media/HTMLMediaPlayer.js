//framework
import BaseMediaPlayer from './BaseMediaPlayer.js';

/*
 * App
 *
 */
export default class HTMLMediaPlayer extends BaseMediaPlayer {
	
	constructor (tag) {
		//call parent
		super();

		//params
		this._tag = tag;
	}

	// Getters And Setters
	//----------------------------------------------------------
	get duration () {
		if(this._tag) {
			return this._tag.duration;
		}
	}
	get currentTime () {
		if(this._tag) return this._tag.currentTime;
	}
	set currentTime (value) {
		if(this._tag) {
			this._tag.currentTime = value;
			this._updateTime();
		}
	}
	
	get volume () {}
	set volume (value) {this._setVolume(value)}

	// Public
	//-------------------------------------------------------
	change (source = "") {
		
	}

	play () {
		if(this._tag) {
			this._tag.play();
			super.play();
		}
	}

	pause () {
		if(this._tag) {
			this._tag.pause();
			super.pause();
		}
	}

	// Private
	//-------------------------------------------------------
	_setVolume (value) {
		
	}
}