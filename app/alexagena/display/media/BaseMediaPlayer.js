//framework
import TickController from '~fr/controllers/TickController.js';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * App
 *
 */
export default class BaseMediaPlayer extends EventDispatcher {
	
	constructor () {
		//call parent
		super();

		//params
		this._ticking = false;

		//bind
		this._updateTime = this._updateTime.bind(this);
	}
	
	static get PLAY () {return "BaseMediaPlayerPlay"}
	static get PAUSE () {return "BaseMediaPlayerPause"}
	static get UPDATE_TIME () {return "BaseMediaPlayerUpdateTime"}

	// Getters And Setters
	//----------------------------------------------------------
	get totalTime () {}
	get currentTime () {}
	get progress () {}
	
	get volume () {}
	set volume (value) {this._setVolume(value)}

	// Public
	//-------------------------------------------------------
	load () {

	}

	play () {
		if(!this._ticking) {
			this._ticking = true;
			TickController.subscribe(this._updateTime);
		}
		this.trigger(BaseMediaPlayer.PLAY);
	}

	pause () {
		if(this._ticking) {
			this._ticking = false;
			TickController.unsubscribe(this._updateTime);
		}
		this.trigger(BaseMediaPlayer.PAUSE);
	}

	seek () {
		
	}

	// Private
	//-------------------------------------------------------
	_setVolume (value) {

	}

	_updateTime () {
		this.trigger(BaseMediaPlayer.UPDATE_TIME, this.currentTime);
	}
}
