//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import BaseMediaPlayer from './BaseMediaPlayer.js';
import HTMLMediaPlayer from './HTMLMediaPlayer.js';
import MediaControls from './MediaControls.jsx';
import styles from './ReactPlayer.scss';

/*
 * ReactPlayer
 *
 */
export default class ReactPlayer extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//parms
		this._player;

		//state
		this.state.playing = false;
		this.state.duration = 0;
		this.state.currentTime = 0;
		this.addClass('react-player');

		//bind
		this._onVideoPlay = this._onVideoPlay.bind(this);
		this._onVideoPause = this._onVideoPause.bind(this);
		this._onVideoTimeUpdate = this._onVideoTimeUpdate.bind(this);
		this._onPlayClicked = this._onPlayClicked.bind(this);
		this._onPauseClicked = this._onPauseClicked.bind(this);
		this._onSeekRequested = this._onSeekRequested.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			source: PropTypes.string
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}

	get player () {return this._player;}

	// React
	//-------------------------------------------------------
	render (childs) {
		let {source} = this.props;
		let {duration, currentTime} = this.state;
		return (
			<div ref="wrapper" class={this.state.className}>
				<video ref="video" src={source} />
				<div class="controls-container">
					<MediaControls
						onPlay={this._onPlayClicked}
						onPause={this._onPauseClicked}
						onSeek={this._onSeekRequested}
						isPlaying={this.state.playing}
						totalTime={duration}
						currentTime={currentTime}
					/>
				</div>
			</div>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		this._player = new HTMLMediaPlayer(this.refs.video);
		this._player.on(BaseMediaPlayer.PLAY, this._onVideoPlay);
		this._player.on(BaseMediaPlayer.PAUSE, this._onVideoPause);
		this._player.on(BaseMediaPlayer.UPDATE_TIME, this._onVideoTimeUpdate);
	}

	// Private
	//-------------------------------------------------------
	_onVideoPlay () {
		this.setState((state)=>{
			return {...state, playing: true}
		});
	}

	_onVideoPause () {
		this.setState((state)=>{
			return {...state, playing: false}
		});
	}

	_onVideoTimeUpdate (e, time) {
		this.setState((state)=>{
			return {
				...state,
				duration: this._player.duration,
				currentTime: time
			}
		});
	}

	// Controls
	//-----------------------
	_onPlayClicked () {
		if(this._player) {
			this._player.play();
		}
	}

	_onPauseClicked () {
		if(this._player) {
			this._player.pause();
		}
	}

	_onSeekRequested (e, time) {
		if(this._player) {
			this._player.currentTime = time;
		}
	}
}