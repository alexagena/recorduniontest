//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {PlaybackControls, ProgressBar, FormattedTime} from 'react-player-controls';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import styles from './MediaControls.scss';

/*
 * MediaControls
 *
 */
export default class MediaControls extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('media-controls');

		//bind
		this._onPlaybackChange = this._onPlaybackChange.bind(this);
		this._onSeek = this._onSeek.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			isPlaying: PropTypes.bool,
			totalTime: PropTypes.number,
			currentTime: PropTypes.number,
			bufferedTime: PropTypes.number,
			isSeekable: PropTypes.bool,
			onPlay: PropTypes.func,
			onPause: PropTypes.func,
			onSeek: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			totalTime: 0,
			currentTime: 0
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {isPlaying, totalTime, currentTime, bufferedTime} = this.props;

		totalTime = totalTime || 0;
		currentTime = currentTime || 0;
		bufferedTime = bufferedTime || 0;

		return (
			<div ref="wrapper" class={this.state.className}>
				<div class="play-pause-buttons">
					<PlaybackControls
						isPlaying={isPlaying}
						isPlayable={true}
						showPrevious={false}
						showNext={false}
						onPlaybackChange={this._onPlaybackChange}
					/>
				</div>
				<div class="progress-container">
					<FormattedTime
						numSeconds={currentTime}
					/>
					<ProgressBar
						totalTime={totalTime}
						currentTime={currentTime}
						bufferedTime={bufferedTime}
						isSeekable={true}
						onSeek={this._onSeek}
					/>
					<FormattedTime
						numSeconds={totalTime}
					/>
				</div>
			</div>
		);
	}

	// Private
	//-------------------------------------------------------
	_onPlaybackChange (isPlaying) {
		if(isPlaying) {
			this.triggerEvent("onPlay");
		}
		else {
			this.triggerEvent("onPause");
		}
	}

	_onSeek (time) {
		this.triggerEvent("onSeek", {}, time);
	}
}