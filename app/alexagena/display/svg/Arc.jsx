// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';

/*
 * Arc
 *
 */
export default class Arc extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._iniAngle = 0;
		this._endAngle = Math.PI * 2 - 0.001;

		//default class
		this.addClass('arc');
	}

	static get propTypes () {
		return {
			...super.propTypes,
			radius: PropTypes.number,
			fill: PropTypes.string,
			stroke: PropTypes.string,
			strokeWidth: PropTypes.number,
			viewBoxSize: PropTypes.number
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			radius: 100,
			fill: "#000000",
			stroke: "none",
			strokeWidth: 0,
			viewBoxSize: 100
		}
	}

	get iniAngle () {return this._iniAngle}
	set iniAngle (value) {
		this._iniAngle = value;
		this._update();
	}

	get endAngle () {return this._endAngle}
	set endAngle (value) {
		this._endAngle = value;
		this._update();
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {radius, fill, stroke, strokeWidth} = this.props;
		let size = radius * 2 + strokeWidth;
		return (
			<svg ref="wrapper" class={this.state.className} width={radius*2} height={radius*2} viewBox={`0 0 ${size} ${size}`}>
				<g transform={`translate(${radius}, ${radius})`}>
					<path ref="arcPath" fill={fill} stroke={stroke} strokeWidth={strokeWidth} />
				</g>
			</svg>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		this._update();
	}

	// Public
	//-------------------------------------------------------
	changeColor (color) {
		let params = {};
		if(this.props.fill != "none") {
			params.fill = color;
		}
		if(this.props.stroke != "none") {
			params.stroke = color;
		}
		TweenMax.killTweensOf(this.refs.arcPath);
		TweenMax.to(this.refs.arcPath, 0.5, {
			...params,
			ease: Quart.easeInOut
		});
	}

	// Private
	//-------------------------------------------------------
	_update () {
		let {radius, strokeWidth} = this.props;
		let x1 = Math.cos(this.iniAngle) * radius + (strokeWidth / 2);
		let y1 = Math.sin(this.iniAngle) * radius + (strokeWidth / 2);
		let x2 = Math.cos(this.endAngle) * radius + (strokeWidth / 2);
		let y2 = Math.sin(this.endAngle) * radius + (strokeWidth / 2);
		let side = this.endAngle - this.iniAngle >= Math.PI ? 1 : 0;
		let path = `M ${x1} ${y1} A ${radius} ${radius} 0 ${side},1 ${x2} ${y2}`;
		this.refs.arcPath.setAttribute("d", path);
	}
}