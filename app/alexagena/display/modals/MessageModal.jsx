// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseModal from '~fr/display/BaseModal.jsx';
import BaseButton from '~fr/display/BaseButton.jsx';
import LightboxController from '~fr/controllers/LightboxController.jsx';
//styles
import styles from './MessageModal.scss';

/*
 * MessageModal
 *
 */
export default class MessageModal extends BaseModal {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//default class
		this.addClass('message-modal');

		//bind
		this._onCloseClicked = this._onCloseClicked.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			message: PropTypes.string,
			labelClose: PropTypes.string,
			ButtonClass: PropTypes.func,
			onCloseClicked: PropTypes.func
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps,
			message: "",
			labelClose: "Fechar",
			ButtonClass: BaseButton
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {message, labelClose, ButtonClass} = this.props;

		return super.render(
			<div>
				<div class="confirm-content">
					<h2 class="message" dangerouslySetInnerHTML={{__html: message}}></h2>
				</div>
				<footer class="confirm-buttons">
					<ButtonClass label={labelClose} onClick={this._onCloseClicked} />
				</footer>
			</div>
		);
	}

	// Private
	//-------------------------------------------------------
	_onCloseClicked () {
		this.triggerEvent(LightboxController.REQUEST_CLOSE);
	}
}