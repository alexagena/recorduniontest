// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseModal from '~fr/display/BaseModal.jsx';
import BaseButton from '~fr/display/BaseButton.jsx';
import LightboxController from '~fr/controllers/LightboxController.jsx';
//styles
import styles from './ConfirmModal.scss';

/*
 * ConfirmModal
 *
 */
export default class ConfirmModal extends BaseModal {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//default class
		this.addClass('confirm-modal');

		//bind
		this._onNoClicked = this._onNoClicked.bind(this)
		this._onYesClicked = this._onYesClicked.bind(this)
	}

	static get propTypes () {
		return {
			question: PropTypes.string,
			extraInfo: PropTypes.object,
			labelNo: PropTypes.string,
			labelYes: PropTypes.string,
			ButtonClass: PropTypes.object,
			onYesClicked: PropTypes.func,
			onNoClicked: PropTypes.func
		}
	}
	
	static get defaultProps () {
		return {
			question: "",
			labelNo: "NÃO",
			labelYes: "SIM",
			ButtonClass: BaseButton
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		let {question, extraInfo, labelYes, labelNo, ButtonClass} = this.props;

		return super.render(
			<div>
				<div class="confirm-content">
					<h2 class="question" dangerouslySetInnerHTML={{__html: question}}></h2>
					{!!extraInfo && extraInfo}
				</div>
				<div class="confirm-buttons">
					<ButtonClass
						class="primary"
						label={labelYes}
						onClick={this._onYesClicked}
					/>
					<ButtonClass
						label={labelNo}
						onClick={this._onNoClicked}
					/>
				</div>
			</div>
		);
	}

	// Private
	//-------------------------------------------------------
	_onYesClicked (e) {
		this.triggerEvent("onYesClicked", e);
		this.triggerEvent(LightboxController.REQUEST_CLOSE);
	}

	_onNoClicked (e) {
		this.triggerEvent("onNoClicked", e);
		this.triggerEvent(LightboxController.REQUEST_CLOSE);
	}
}