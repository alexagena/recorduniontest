// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
// framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import SyncViewsManager from '~fr/navigation/managers/SyncViewsManager.jsx';

/*
 * BaseView
 * ~fr/display/BaseView.jsx
 */
export default class BaseView extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this.id = (props.config && props.config.id) || '';
		this.content = (props.config && props.config.content) || {};
		this.subviews = [];
		this.parentView = null;
		
		//default class
		this.addClass('base-view');

		//bind
		this.showComplete = this.showComplete.bind(this);
		this.hideComplete = this.hideComplete.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			config: PropTypes.object,
			routeData: PropTypes.object
		}
	}
	
	// Getters and Setters
	//----------------------------------------------------------
	get manager () {
		return this.refs.manager;
	}
	
	// Public
	//-------------------------------------------------------	
	render (childs) {
		return (
			<div ref="wrapper" class={this.state.className}>
				{this.renderManager()}
			</div>
		);
	}
	
	renderManager () {
		return <SyncViewsManager ref="manager" class="subviews-wrapper"></SyncViewsManager>;
	}
}