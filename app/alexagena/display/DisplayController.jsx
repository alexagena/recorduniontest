// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';

/*
 * DisplayController
 *
 */
export default class DisplayController extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this._queue = [];
		this._processing = false;
		this._currentPage = false;
		this._lastAction = '';
		
		//state
		this.state.pages = {};
		this.addClass('display-container');
		
		//bind methods
		this._showPageComplete = this._showPageComplete.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			preserveHeight: PropTypes.bool
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps,
			preserveHeight: false
		}
	}

	get lastAction () {return this._lastAction}
	get currentPage () {return this._currentPage}
	get currentPageRef () {
		if(this._currentPage && this._currentPage.ref) {
			return this.refs[this._currentPage.ref];
		}
	}

	// React
	//-------------------------------------------------------
	render () {
		return (
			<div ref="wrapper" class={this.state.className}>
				{this.renderPages()}
			</div>
		);
	}
	
	renderPages () {
		let array = [];
		let pages = this.state.pages;

		for(let key in pages) {
			let data = pages[key];
			let {Page, props} = data;
			data.ref = "page" + key;
			array.push(<Page ref={data.ref} {...props} />);
		}
		return array;
	}

	componentDidUpdate(prevProps, prevState) {
		if(this._lastAction == 'add') {
			this._lastAction = '';
			this._showCurrentPage();
			this._releaseHeight();
			this.triggerEvent("onNewPageRendered", this._currentPage);
		}
	}
	
	// Public
	//-------------------------------------------------------
	change (Page, props) {
		let child = {Page, props};
		this._queue.push(child);
		this._preserveHeight();
		if(!this._processing) {
			this.showContent(child);
		}
	}
	
	showContent (child) {
		this._processing = true;
		if(this._currentPage) {
			this.removeContent(()=> {
				this._createPage(child);
			});
		}
		else {
			this._createPage(child);
		}
	}
	
	removeContent (callback) {
		this._queue.length = 0;
		if(this._currentPage) {
			let pageRef = this.refs[this._currentPage.ref];
			pageRef.onHideComplete = (event)=> {
				//remove listener
				pageRef.onHideComplete = null;
				//kill reference
				this._currentPage = null;
				//kill current page
				this._killPage(event.target);
				//continue
				if(callback) callback();
			}
			pageRef.prepareToHide();
			pageRef.hide();
		}
	}
	
	kill (id) {
		if(this.state.pages[id]) {
			let ref = this.refs[this.state.pages[id].ref];
			ref && this._killPage(ref, true);
		}
	}
	
	hasContent () {
		return !!this._currentPage;
	}
		
	// Private
	//-------------------------------------------------------
	_createPage (child) {
		this._removePageFromQueue(child);
		let pages = {...this.state.pages};
		pages[child.props.key] = child;
		this._currentPage = child;
		this._lastAction = 'add';
		this.setState((state)=>{return {...state, pages}});
	}
	
	_showCurrentPage () {
		let ref = this.refs[this._currentPage.ref];
		if(ref) {
			ref.onShowComplete = this._showPageComplete;
			ref.prepareToShow();
			ref.show();
		}
	}

	_showPageComplete (e) {
		//clear listener
		let ref = this.refs[this._currentPage.ref];
		ref.onShowComplete = null;
		//check queue
		if(this._queue.length > 0) {
			this.showContent(this._queue[this._queue.length - 1]);
		}
		else {
			this._changeFinished();
		}
	}

	_preserveHeight () {
		let current = this.currentPageRef;
		if (current && this.props.preserveHeight) {
			this.wrapper.style.height = current.wrapHeight + "px"; 
		}
	}

	_releaseHeight () {
		let current = this.currentPageRef;
		if (current && this.props.preserveHeight) {
			this.wrapper.style.height = ""; 
		}
	}

	_killPage (target, force = false) {
		//trigger removed
		this.triggerEvent('onPageRemoved', target);
		//search to kill
		let pages = {...this.state.pages};
		for(let key in pages) {
			let page = pages[key];
			let pageRef = this.refs[page.ref];
			if(pageRef == target && (page.props.shouldKill == undefined || page.props.shouldKill || force)) {
				pageRef.destroy();
				pages[key] = null;
				delete pages[key];
				this._lastAction = 'kill';
				this.setState((state)=>{return {...state, pages}});
				this.triggerEvent('onKilled', page);
			}
		}
	}
	
	_removePageFromQueue (child) {
		for(let i = 0; i < this._queue.length; i++) {
			let page = this._queue[i];
			if(page == child) {
				this._queue.splice(i, 1);
			}
		}
	}

	_changeFinished () {
		this._processing = false;
		this.triggerEvent('onChangeComplete');
	}
}