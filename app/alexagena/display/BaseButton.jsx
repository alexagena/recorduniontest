//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import StringUtils from '~fr/utils/StringUtils.js';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//styles
import styles from './BaseButton.scss';

/*
 * BaseButton
 * ~fr/display/BaseButton.jsx
 */
export default class BaseButton extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._enabled = true;
		this._selected = false;
				
		//default class
		this.addClass('button');

		//bind
		this._click = this._click.bind(this);
		this._over = this._over.bind(this);
		this._out = this._out.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			element: PropTypes.string,
			index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			href: PropTypes.string,
			label: PropTypes.string,
			title: PropTypes.string,
			target: PropTypes.string,
			labelLimit: PropTypes.number,
			selected: PropTypes.bool,
			onClick: PropTypes.func,
			onMouseEnter: PropTypes.func,
			onMouseLeave: PropTypes.func
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps,
			element: "a",
			href: "",
			label: "",
			title: "",
			target: "",
			labelLimit: null
		}
	}
	
	// Getters and Setters
	//--------------------------------------------------------
	static get CLICK () {return 'onBaseButtonClicked';}
	static get OVER () {return 'onBaseButtonOver';}
	static get OUT () {return 'onBaseButtonOut';}

	// React
	//-------------------------------------------------------
	componentWillMount() {
		super.componentWillMount();
		this.href = this._fixHref(this.props.href);
	}
	
	componentWillUpdate(nextProps, nextState) {
		this.href = this._fixHref(nextProps.href);
	}
	
	render (childs) {
		let hasChilds = !!childs;
		let labelLimit = this.props.labelLimit || 99999;
		let {title, href, target, label} = this.props;
		let aProps = {}
		
		//setup props
		if(href) {aProps.href = href}
		if(title) {aProps.title = title}
		if(target) {aProps.target = target}
		
		//render
		return (
			<this.props.element ref="wrapper" class={this.state.className} {...aProps} onClick={this._click} onMouseEnter={this._over} onMouseLeave={this._out}>
				{hasChilds && 
					childs
				}
				{!hasChilds && label &&
					<span class="label">{StringUtils.limit(label, labelLimit)}</span>
				}
			</this.props.element>
		);
	}

	componentDidMount () {
		super.componentDidMount();
		this._checkSelected();
	}

	componentDidUpdate () {
		this._checkSelected();
	}

	// Public
	//--------------------------------------------------------
	click (e = {}) {
		if(this._enabled) {
			//trigger click
			this.triggerEvent("onClick", e);
			//check router setup
			let p = this.props;
			if(!e.metaKey && p.target != '_blank' && p.href && p.href.indexOf('http') == -1) {
				e.preventDefault();
				app.main.router.change(p.href);
			}
		}
	}
	
	over (e) {
		if(this._enabled && !this._selected) {
			this.triggerEvent("onMouseEnter", e);
		}
	}
	
	out (e) {
		if(this._enabled && !this._selected) {
			this.triggerEvent("onMouseLeave", e);
		}
	}
	
	select () {
		this._selected = true;
		this.addClass('selected');
	}

	deselect () {
		this.removeClass('selected');
		this._selected = false;
		this.out();
	}
	
	disable () {
		this._enabled = false;
		this.addClass('disable', true);
	}
	
	enable () {
		this._enabled = true;
		this.removeClass('disable');
	}
	
	// Private
	//--------------------------------------------------------
	_fixHref (href = "") {
		if(href) {
			if(href.indexOf('://') == -1 && href.indexOf('{root}') == -1) {
				href = '{root}' + href;
			}
			href = href.replace('{root}', '');
		}
		return href;
	}

	_checkSelected () {
		if(typeof(this.props.selected) == "boolean") {
			if(this.props.selected) {
				this.select();
			}
			else {
				this.deselect();
			}
		}
	}

	_click (e) {
		if(this._enabled) {
			this.click(e);
		}
		else {
			if(e.preventDefault) {
				e.preventDefault();
			}
		}
	}
	
	_over (e) {
		this.over(e);
	}
	
	_out (e) {
		this.out(e);
	}
}