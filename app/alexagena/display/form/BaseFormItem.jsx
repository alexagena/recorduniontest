// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import FormUtils from '~fr/utils/FormUtils.js';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';

/*
 * BaseFormItem
 *
 */
export default class BaseFormItem extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//state
		this.state.value = props.initialValue;
		
		//default class
		this.addClass('base-form-item');
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			name: PropTypes.string,
			validations: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
			initialValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			validations: [],
			initialValue: ""
		}
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	get value () {return this.state.value;}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		return (
			<div ref="wrapper" class={this.state.className}></div>
		);
	}
	
	// Public
	//-------------------------------------------------------
	validate () {
		let props = this.props;
		let list = typeof(props.validations) == "string" ? props.validations.split(" ") : props.validations;
		let valid = FormUtils.validate(list, this.value);
		this.setError(!valid.result);
		return valid;
	}
	
	setError (hasError = true) {
		this[hasError ? "addClass" : "removeClass"]('error');
	}

	serialize () {
		let data = {};
		if(this.props.name) {
			data[this.props.name] = this.value;
		}
		return data;
	}

	focus () {
		if(this.refs.input) {
			this.refs.input.focus();
		}
		else if (this.refs.wrapper) {
			this.refs.wrapper.focus();
		}
	}

	blur () {
		if(this.refs.input) {
			this.refs.input.blur();
		}
		else if (this.refs.wrapper) {
			this.refs.wrapper.blur();
		}
	}

	clear () {
		this.setState({value: ""});
		this.setError(false);
	}
}