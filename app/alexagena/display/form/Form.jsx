//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import styles from './Form.scss';

/*
 * Form
 * ~fr/display/form/Form.jsx
 */
export default class Form extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this._namesList = [];

		//state
		this.addClass('form');

		//bind
		this._onKeyDown = this._onKeyDown.bind(this);
		this._onTypeFinished = this._onTypeFinished.bind(this);
	}

	static get propTypes () {
		return {
			...super.propTypes,
			factory: PropTypes.object,
			rowsFields: PropTypes.array
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps,
			rowsFields: []
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		return (
			<form ref="wrapper" class={this.state.className} onKeyDown={this._onKeyDown}>
				{this.renderRows(this.props.rowsFields)}
			</form>
		);
	}

	renderRows (rowsFields) {
		//clear names list
		this._namesList.length = 0;
		//parse rows
		return rowsFields.map((fields, i)=>{
			return this.renderFields(fields, i);
		});
	}

	renderFields (fields, index) {
		let renderList = [];
		//parse fields
		for(let i = 0; i < fields.length; i++) {
			let field = fields[i];
			let width = ((field.size || 1) * 100) + "%";
			let Input = this.props.factory.make(field.type);
			if(field.name) {
				if(this._namesList.indexOf(field.name) == -1) {
					//cache new name
					this._namesList.push(field.name);
					//push new input
					renderList.push(
						<div key={"input" + i} class="input-item" style={{width}}>
							<Input ref={"input" + field.name} {...field} onTypeFinished={this._onTypeFinished} />
						</div>
					);
				}
				else {
					console.log('Ambiguous name property', field.name);
				}
			}
			else {
				console.log('Field name is required', field);				
			}
		}
		//render
		return (
			<fieldset key={"row" + index} class="fieldset-row">
				{renderList}
			</fieldset>
		);
	}

	// Public
	//-------------------------------------------------------
	eachInput (callback) {
		for(let i = 0; i < this._namesList.length; i++) {
			let name = this._namesList[i];
			let item = this.refs["input" + this._namesList[i]];
			let keep = true;
			if(callback) {
				if(callback(item, name, i) === false) {
					keep = false;
				}
			}
			if(!keep) {
				break;
			}
		}
	}

	getItemByName (name) {
		let input = this.refs["input" + name];
		return input;
	}

	getNext (target) {
		let found = false;
		let result = null;
		let isHtmlEl = target instanceof HTMLElement;
		this.eachInput((item, name, i)=>{
			if(found) {
				result = item;
				return false;
			}
			if(isHtmlEl && item.refs.input == target) {
				found = true;
			}
			else if (item == target) {
				found = true;
			}
		});
		return result;
	}

	focusOnNext (target) {
		let next = this.getNext(target);
		next ? next.focus() : target.blur();
	}

	validate () {
		let itemsError = [];
		for(let i = 0; i < this._namesList.length; i++) {
			let input = this.getItemByName(this._namesList[i]);
			let validation = input.validate();
			if(!validation.result) {
				itemsError.push({
					input, validation
				});
			}
		}
		return {items: itemsError, result: !itemsError.length};
	}

	serialize () {
		let result = {};
		for(let i = 0; i < this._namesList.length; i++) {
			let input = this.getItemByName(this._namesList[i]);
			result = {
				...result,
				...input.serialize()
			}
		}
		return result;
	}

	clear () {
		for(let i = 0; i < this._namesList.length; i++) {
			let input = this.getItemByName(this._namesList[i]);
			if (input && input.clear) {
				input.clear();
			}
		}
	}

	// Private
	//-------------------------------------------------------
	_onTypeFinished (e) {
		this.focusOnNext(e.target);
	}
	
	_onKeyDown (e) {
		if(e.keyCode == 13 || e.keyCode == 9) {
			this.focusOnNext(document.activeElement);
		}
		if(e.keyCode == 9) {
			e.preventDefault();
		}
	}
}