//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//project
import Icon from '~fr/display/icons/Icon.jsx';
//styles
import styles from './SVGIcon.scss';

/*
 * SVGIcon
 * ~fr/display/icons/SVGIcon.jsx
 */
export default class SVGIcon extends Icon {

	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('svg-icon');
	}

	static analyseData (data = {}) {
		return !!data.svg;
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			id: PropTypes.string,
			svg: PropTypes.string,
			showCircle: PropTypes.bool
		}
	}
	
	static get defaultProps () {
		return {
			...super.defaultProps,
			id: "",
			svg: "",
			showCircle: false
		}
	}
	
	// React
	//-------------------------------------------------------
	render () {
		return (
			<span ref="wrapper" class={this.state.className} data-id={this._id}>
				<div class="svg" dangerouslySetInnerHTML={{__html: this._data.svg}}></div>
			</span>
		);
	}
}