// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
// framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';

/*
 * Icon
 *
 */
export default class Icon extends BaseDisplay {

	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('icon');
	}
	
	static getDataFromId (p_id = '') {
		let content = app.main.content;
		let result = content && content.assets && content.assets.icons && content.assets.icons.result && content.assets.icons.result[p_id];
		if (result) {
			return content.assets.icons.result[p_id];
		}
		return {};
	}

	static get propTypes () {
		return {
			...super.propTypes,
			data: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps
		}
	}
	
	// React
	//-------------------------------------------------------
	componentWillMount() {
		this._parseData(this.props.data)
	}

	componentWillUpdate(nextProps, nextState) {
		this._parseData(nextProps.data)
	}
	
	render (childs) {
		return (
			<span ref="wrapper" class={this.state.className}></span>
		);
	}
	
	// Private
	//-------------------------------------------------------
	_parseData (data) {
		if(typeof(data) == 'string') {
			this._id = data;
			this._data = Icon.getDataFromId(data);
		}
	}
}