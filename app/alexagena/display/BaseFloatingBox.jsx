// third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseButton from '~fr/display/BaseButton.jsx';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//styles
import styles from './BaseFloatingBox.scss';

/*
 * BaseFloatingBox
 *
 */
export default class BaseFloatingBox extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this.shown = false;
		this.isPrepared = false;
		
		//private
		this._positionMode = "top";
		
		//default class
		this.addClass('base-floating-box');
		
		//bind methods
		this._keepVisible = this._keepVisible.bind(this);
		this._setTimerToHide = this._setTimerToHide.bind(this);
		this._toggleVisibility = this._toggleVisibility.bind(this);
	}
	
	static get propTypes () {
		return {
			actionButton: PropTypes.oneOfType([
				PropTypes.instanceOf(HTMLElement),
				PropTypes.instanceOf(BaseButton)
			]),
			onMouseEnter: PropTypes.func,
			onMouseLeave: PropTypes.func
		}
	}

	static get defaultProps () {
		return {
			autoShow: true
		}
	}

	// Getters and Setters
	//-------------------------------------------------------
	set positionMode (value) {
		if(value == 'bottom') {
			this.addClass('bottom');
			this._positionMode = value;
		}
		else {
			this.removeClass('bottom');
			this._positionMode = "top";
		}
	}

	// React
	//-------------------------------------------------------
	render (childs) {
		return (
			<div ref="wrapper" class={this.state.className} onMouseEnter={this._keepVisible} onMouseLeave={this._setTimerToHide}>
				{childs && childs}
				<div ref="markerArrow" class="arrow-box-marker"></div>
			</div>
		);
	}
		
	componentDidMount() {
		super.componentDidMount();
		this.prepareToShow();
	}

	// Public
	//-------------------------------------------------------
	onBoxCreated () {
		if(this.props.autoShow) {
			this.show();
		} else {
			this.hide(0, 0, true);
		}
		//add same actions from box to button
		if(this.props.actionButton) {
			let actionButton = this.props.actionButton;
			if(actionButton instanceof BaseButton) {
				actionButton = this.props.actionButton.refs.wrapper;
			}
			actionButton.addEventListener('click', this._toggleVisibility);
			actionButton.addEventListener('mouseenter', this._keepVisible);
			actionButton.addEventListener('mouseleave', this._setTimerToHide);
		}
		//trigger ready
		this.triggerEvent("onBoxReady");
	}
	
	prepareToShow () {
		if(!this.isPrepared) {
			this.isPrepared = true;
			TweenMax.set(this.refs.wrapper, {
				scale: 1,
				opacity: 0,
				display: ''
			});
		}
	}

	show (p_delay = 0, p_factor = 1) {
		if(!this.shown) {
			this.shown = true;
			let isBottom = this._positionMode == "bottom";
			TweenMax.killTweensOf(this.refs.wrapper);
			TweenMax.set(this.refs.wrapper, {y: 25  * (isBottom ? -1 : 1)})
			TweenMax.to(this.refs.wrapper, 0.4, {
				y: 0,
				opacity: 1,
				force3D: true,
				ease: Back.easeOut
			});
			TweenMax.killTweensOf(this.refs.markerArrow);
			TweenMax.set(this.refs.markerArrow, {y: 25  * (isBottom ? -1 : 1)})
			TweenMax.to(this.refs.markerArrow, 0.3, {
				y: 0,
				ease: Quart.easeInOut,
				delay: 0.1,
				force3D: true,
				onComplete: super.show.bind(this)
			});
			this.isPrepared = false;
		}
	}

	hide (p_delay = 0, p_factor = 1, p_force = false) {
		if(this.shown || p_force) {
			this.shown = false;
			TweenMax.killTweensOf(this.refs.wrapper);
			TweenMax.to(this.refs.wrapper, 0.3, {
				scale: 0.95,
				opacity: 0,
				force3D: true,
				ease: Quart.easeInOut,
				onComplete: super.hide.bind(this)
			});
			this.isPrepared = false;
		}
	}

	destroy () {
		if(this.props.actionButton) {
			let actionButton = this.props.actionButton;
			if(this.props.actionButton instanceof BaseButton) {
				actionButton = this.props.actionButton.refs.wrapper;
			}
			actionButton.removeEventListener('click', this._toggleVisibility);
			actionButton.removeEventListener('mouseenter', this._keepVisible);
			actionButton.removeEventListener('mouseleave', this._setTimerToHide);
		}
	}
	
	// Private
	//-------------------------------------------------------
	_toggleVisibility (e) {
		if(this.shown) {
			this.hide()
		}
		else {
			this.prepareToShow();
			app.main.floatingBox.fixPosition(this.props.internalKey);
			this.show();
		}
	}
	
	_keepVisible (e) {
		setTimeout(()=> {
			if(this.hideTimer) {
				clearTimeout(this.hideTimer);
			}
		});
	}
	
	_setTimerToHide (e) {
		if(e.target instanceof HTMLSelectElement) {
			return;
		}
		if(this.hideTimer) {
			clearTimeout(this.hideTimer);
		}
		this.hideTimer = setTimeout(this.hide.bind(this), 500);
	}
}