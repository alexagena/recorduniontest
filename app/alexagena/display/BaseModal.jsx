//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
//relative
import style from './BaseModal.scss';

/*
 * BaseModal
 *
 */
export default class BaseModal extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//default class
		this.addClass('base-modal');
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		return (
			<div ref="wrapper" class={this.state.className}>
				{childs}
			</div>
		);
	}
	
	componentDidMount() {
		super.componentDidMount();
		this.prepareToShow();
		setTimeout(this.resize.bind(this), 0);
	}

	// Public
	//-------------------------------------------------------
	prepareToShow () {
		TweenMax.set(this.refs.wrapper, {
			y: 50,
			opacity: 0,
			force3D: true
		});
	}

	show (p_delay = 0, p_factor = 1) {
		TweenMax.to(this.refs.wrapper, 0.3 * p_factor, {
			y: 0,
			opacity: 1,
			ease: Quart.easeInOut,
			delay: p_delay * p_factor,
			force3D: true,
			clearProps: "transform",
			onComplete: this.showComplete
		});
	}

	hide (p_delay = 0, p_factor = 1) {
		TweenMax.to(this.refs.wrapper, 0.3 * p_factor, {
			scale: 0.92,
			opacity: 0,
			ease: Quart.easeInOut,
			delay: p_delay * p_factor,
			force3D: true,
			clearProps: "transform",
			onComplete: this.hideComplete
		});
	}

	resize () {
		if(this.wrapHeight > window.innerHeight) {
			this.addClass("higher-mode");
			this.wrapper.style.top = ""; 
		}
		else {
			this.removeClass("higher-mode");
			this.wrapper.style.top = ((window.innerHeight - this.wrapHeight) / 2) + "px";
		}
		this.wrapper.style.left = ((window.innerWidth - this.wrapWidth) / 2) + "px";
	}
}