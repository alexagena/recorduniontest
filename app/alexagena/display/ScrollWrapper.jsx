//third party
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import TinyScrollbar from '~fr/third-party/tinyscrollbar.js';
//framework
import Detections from '~fr/utils/Detections.js';
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import ResizeController from '~fr/controllers/ResizeController.js';
//styles
import styles from './ScrollWrapper.scss';

/*
 * ScrollWrapper
 * ~fr/display/ScrollWrapper.jsx
 */
export default class ScrollWrapper extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//params
		this._ticker = null;
		this._movingSpeed = false;
		
		//default class
		this.addClass('scroll-wrapper');
		
		//bind methods
		this._onResize = this._onResize.bind(this);
		this._onScroll = this._onScroll.bind(this);
		this._onMouseEnter = this._onMouseEnter.bind(this);
		this._onMouseLeave = this._onMouseLeave.bind(this);
		this._onBarMouseEnter = this._onBarMouseEnter.bind(this);
		this._onBarMouseLeave = this._onBarMouseLeave.bind(this);
		this._onScrollDrag = this._onScrollDrag.bind(this);
		this._onScrollLeave = this._onScrollLeave.bind(this);
	}
	
	static get propTypes () {
		return {
			...super.propTypes,
			use3D: PropTypes.bool,
			autoUpdate: PropTypes.bool,
			wheelSpeed: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			keepScrollVisible: PropTypes.bool,
			keepVisibleOnEnter: PropTypes.bool,
			onScroll: PropTypes.func
		}
	}

	static get defaultProps () {
		return {
			...super.defaultProps,
			use3D: true,
			autoUpdate: false,
			wheelSpeed: "default",
			keepScrollVisible: false,
			keepVisibleOnEnter: true
		}
	}
	
	// React
	//-------------------------------------------------------
	render (childs) {
		return (
			<div ref="wrapper" class={this.state.className} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} onScroll={this._onScroll}>
				<div ref="bar" class="scrollbar" onMouseEnter={this._onBarMouseEnter} onMouseLeave={this._onBarMouseLeave}>
					<div class="track">
						<div class="thumb">
							<div class="end"></div>
						</div>
					</div>
				</div>
				<div ref="viewport" class="viewport">
					<div ref="overview" class="overview">
						{this.props.children}
					</div>
				</div>
			</div>
		);
	}
	
	componentDidMount() {
		super.componentDidMount();
		if(Detections.desktop) {
			this._scrollbar = TinyScrollbar(this.wrapper, {
				use3D: this.props.use3D,
				wheelSpeed: this._getWheelSpeed()
			});
			this.showScrollbar();
			this.wrapper.addEventListener('move', this._onScroll);
		}
		else {
			this.addClass('native-scroll');
		}
		setTimeout(this._onResize, 0);
		ResizeController.on(ResizeController.RESIZE, this._onResize);
	}
	
	componentDidUpdate(prevProps, prevState) {
		if(this.props.autoUpdate) {
			this.updateScroll();
		}
	}
	
	componentWillUnmount () {
		this.wrapper.removeEventListener('move', this._onScroll);
		ResizeController.off(ResizeController.RESIZE, this._onResize);
	}
	
	// Public
	//-------------------------------------------------------
	updateScroll (type = 'relative') {
		//update scroll
		if(this._scrollbar) {
			this._scrollbar.update(type);
		}
		else {
			if(typeof(type) == "number") {
				this.refs.viewport.scrollTop = type; 
			}
		}
		//validate if scroll is active
		if(this.refs.overview.offsetHeight < this.wrapHeight) {
			this.addClass('disable');
		}
		else {
			this.removeClass('disable');
		}
	}

	gotoBottom () {
		if(this._scrollbar) {
			this.updateScroll("bottom");
		}
		else {
			let diff = this.refs.overview.offsetHeight - this.wrapHeight;
			this.refs.viewport.scrollTop = Math.max(diff, 0);
		}
	}

	getScrollTop () {
		if(this._scrollbar) {
			return this._scrollbar.currentPosition;
		}
		else {
			return this.refs.viewport.scrollTop;
		}
	}

	scrollTo (top = 0, time = 0.5, callback) {
		//bottom limit
		let diff = this.refs.overview.offsetHeight - this.wrapHeight;
		top = typeof(top) == "string" ? this._getTopFromSelector(top) : top;
		top = Math.max(Math.min(top, diff), 0);

		//avoid duplicate
		if(!this._scrolling) {
			this._scrolling = true;
			let data = {top: this.getScrollTop()}
			let timeFactor = 1;//Math.min(Math.abs(top - data.top) / 200, 1);
			TweenMax.to(data, time * timeFactor, {
				top: top,
				ease: Quart.easeInOut,
				onUpdate: ()=>{
					this._onScroll();
					this.updateScroll(data.top);
				},
				onComplete:()=>{
					this._scrolling = false;
					callback && callback();
				}
			});
		}
	}

	showScrollbar (keep = false) {
		this._clearClockToHide();
		if(this.refs.bar) {
			TweenMax.killTweensOf(this.refs.bar);
			TweenMax.to(this.refs.bar, 0.5, {
				opacity: 1,
				force3D: true,
				ease: Quart.easeOut,
				onComplete: ()=> {
					if(!keep && !this.props.keepScrollVisible) {
						this._startClockToHide();
					}
				}
			});
		}
	}

	hideScrollbar () {
		if(this.refs.bar) {
			TweenMax.killTweensOf(this.refs.bar);
			TweenMax.to(this.refs.bar, 0.5, {
				opacity: 0,
				force3D: true,
				ease: Quart.easeOut
			});
		}
	}

	startMoving (speed = 1) {
		if(!this._ticker) {
			this._ticker = setInterval(this._updatePosition.bind(this), 1000/30);
			this._maxScrollY = this._scrollbar.contentSize - this._scrollbar.viewportSize;
		}
		this._movingSpeed = speed;
	}

	stopMoving () {
		if(this._ticker) {
			clearInterval(this._ticker);
			this._ticker = false;
		}
		this._movingSpeed = 0;
	}

	getContentHeight () {
		return this.refs.overview.offsetHeight;
	}

	// Private
	//-------------------------------------------------------
	_onMouseEnter () {
		this.showScrollbar(this.props.keepVisibleOnEnter);
	}

	_onMouseLeave () {
		if(!this.props.keepScrollVisible) {
			this._startClockToHide();
		}
	}

	_onBarMouseEnter () {
		this.showScrollbar(true);
	}

	_onBarMouseLeave () {
		if(!this.props.keepScrollVisible && !this.props.keepVisibleOnEnter) {
			this._startClockToHide();
		}
	}

	_onScrollDrag () {
		if(this.refs.thumb) {
			this.refs.thumb.addClass('hover');
			window.addEventListener("mouseup", this._onScrollLeave)
		}
	}

	_onScrollLeave () {
		if(this.refs.thumb) {
			this.refs.thumb.removeClass('hover');
			window.removeEventListener("mouseup", this._onScrollLeave)
		}
	}

	_getTopFromSelector (selector) {
		if(selector) {
			let el = this.wrapper.querySelector(selector);
			if (el) {
				return el.offsetTop;
			}
		}
		return 0;
	}

	_getWheelSpeed () {
		let speed = this.props.wheelSpeed;
		if(speed == "auto") {
			speed = Math.max(this.wrapper.offsetHeight * 0.11, 10);
		}
		if(speed == "default" || !speed) {
			speed = 40;
		}
		return speed;
	}

	_startClockToHide () {
		this._clearClockToHide();
		this._hideScrollTimer = setTimeout(this.hideScrollbar.bind(this), 1500);
	}

	_clearClockToHide () {
		if(this._hideScrollTimer) {
			clearTimeout(this._hideScrollTimer);
		}
	}

	_updatePosition () {
		let pos = this.getScrollTop() + this._movingSpeed;
		pos = Math.max(pos, 0);
		pos = Math.min(pos, this._maxScrollY);
		this.showScrollbar();
		this.updateScroll(pos);
	}

	_onResize () {
		this.updateScroll();
	}

	_onScroll (e) {
		this.triggerEvent('onScroll', {}, this.getScrollTop());
	}
}