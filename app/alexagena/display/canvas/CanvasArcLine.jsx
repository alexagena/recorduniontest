//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasArcLine
 *
 */
export default class CanvasArcLine extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//create element
		this.el = new createjs.Shape();
		this.container = new createjs.Container();

		//private vars
		this._color = "#000";
		this._angle = 0;
		this._radius = 1;
		this._holeSize = 0;
		this._paddingTop = 5;
		this._paddingBottom = 5;
		this._strokeWidth = 2;
	}

	// Getters and Setters
	//-------------------------------------------------------
	get color () {return this._color;}
	set color (value) {this._color = value;}

	get angle () {return this._angle;}
	set angle (value) {this._angle = value;}

	get radius () {return this._radius;}
	set radius (value) {this._radius = value;}

	get holeSize () {return this._holeSize;}
	set holeSize (value) {this._holeSize = value;}

	get paddingTop () {return this._paddingTop;}
	set paddingTop (value) {this._paddingTop = value;}

	get paddingBottom () {return this._paddingBottom;}
	set paddingBottom (value) {this._paddingBottom = value;}

	// Public
	//-------------------------------------------------------
	create () {
		this.el.x = 0;
		this.el.y = 0;
		this.container.addChild(this.el);
	}

	draw () {
		if(!this.radius) return;
		this.el.graphics.clear();
		this.el.graphics.setStrokeStyle(this._strokeWidth);
		this.el.graphics.beginStroke(this.color);

		let cos = Math.cos(this.angle);
		let sin = Math.sin(this.angle);
		let border = this.radius * this.holeSize;

		this.el.graphics.moveTo(cos * (border - this.paddingBottom), sin * (border - this.paddingBottom));
		this.el.graphics.lineTo(cos * (this.radius + this.paddingTop), sin * (this.radius + this.paddingTop));
		this.el.graphics.endStroke();
	}

	destroy () {
		super.destroy();
		this.el.graphics.clear();
		this.container.removeChild(this.el);
	}
}