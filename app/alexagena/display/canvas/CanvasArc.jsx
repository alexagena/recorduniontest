//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasArc
 *
 */
export default class CanvasArc extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//create element
		this.el = new createjs.Shape();

		//private vars
		this._color = 0;
		this._radius = 1;
		this._holeSize = 0;
		this._iniAngle = 0;
		this._endAngle = 0;
	}

	// Getters and Setters
	//-------------------------------------------------------
	get color () {return this._color;}
	set color (value) {this._color = value;}

	get radius () {return this._radius;}
	set radius (value) {this._radius = value;}

	get holeSize () {return this._holeSize;}
	set holeSize (value) {this._holeSize = value;}

	get iniAngle () {return this._iniAngle;}
	set iniAngle (value) {this._iniAngle = value;}
	
	get endAngle () {return this._endAngle;}
	set endAngle (value) {this._endAngle = value;}

	// Public
	//-------------------------------------------------------
	create () {
		this.el.x = 0;
		this.el.y = 0;
		this.container.addChild(this.el);
		this.container.addEventListener('mouseout', this._onMouseOut.bind(this));
		this.container.addEventListener('mouseover', this._onMouseOver.bind(this));
	}

	draw () {
		if(!this.radius) return;
		let border = this.radius - (this.radius * this.holeSize);
		this.el.graphics.clear();
		if(this.holeSize) {
			this.el.graphics.setStrokeStyle(border);
			this.el.graphics.beginStroke(this.color);
			this.el.graphics.arc(1, 1, this.radius - (border / 2), this.iniAngle, this.endAngle);
			this.el.graphics.endStroke();
		}
		else {
			this.el.graphics.beginFill(this.color);
			this.el.graphics.moveTo(0, 0);
			this.el.graphics.arc(1, 1, this.radius, this.iniAngle, this.endAngle);
			this.el.graphics.endFill();
			this.el.graphics.closePath();
		}
	}
	
	destroy () {
		super.destroy();
		this.el.graphics.clear();
		this.container.removeChild(this.el);
	}

	// Private
	//-------------------------------------------------------
	_onMouseOut () {
		this.trigger(BaseCanvasItem.MOUSE_OUT);
	}
	
	_onMouseOver () {
		this.trigger(BaseCanvasItem.MOUSE_OVER);
	}
}