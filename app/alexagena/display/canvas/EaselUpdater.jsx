// third party
import React from 'react';
import ReactDOM from 'react-dom';
//framework
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * EaselUpdater
 *
 */
class EaselUpdater extends EventDispatcher {
	
	constructor (props = {}) {
		//call parent
		super();
		
		//params
		this.props = {...EaselUpdater.defaultProps, ...props};
		this.items = [];
		this.request = null;
		this.running = false;
		
		//bind
		this.start = this.start.bind(this);
	}

	static get defaultProps () {
		return {

		}
	}
	
	// Public
	//-------------------------------------------------------
	subscribe (item) {
		if(item) {
			for(let i = 0; i < this.items.length; i++) {
				if(item == this.items[i]) {
					break;
					return;
				}
			}
			if(!this.running) {
				this.start();
			}
			this.items.push(item);
		}
	}
	
	unsubscribe (item) {
		if(item) {
			for(let i = 0; i < this.items.length; i++) {
				if(item == this.items[i]) {
					this.items.splice(i, 1);
					break;
				}
			}
			if(!this.items.length) {
				this.stop();
			}
		}
	}

	start () {
		for(let i = 0; i < this.items.length; i++) {
			if(this.items[i].canUpdate) {
				this.items[i].update();
				this.items[i].canUpdate = false;
			}
		}
		this.running = true;
		this.request = window.requestAnimationFrame(this.start);
	}
	
	stop () {
		this.running = false;
		window.cancelAnimationFrame(this.request);
	}
}

export default new EaselUpdater();