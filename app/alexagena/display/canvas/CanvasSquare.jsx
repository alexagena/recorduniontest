//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasSquare
 *
 */
export default class CanvasSquare extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._width = 0;
		this._height = 0;

		//create element
		this.el = new createjs.Shape();
		this.container = new createjs.Container();
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	get color () {return this._color;}
	set color (value) {this._color = value;}

	get width () {return this._width;}
	set width (value) {this._width = value;}
	
	get height () {return this._height;}
	set height (value) {this._height = value;}

	// Public
	//-------------------------------------------------------
	create () {
		this.el.x = 0;
		this.el.y = 0;
		this.container.addChild(this.el);
	}

	draw () {
		this.el.graphics.clear();
		this.el.graphics.beginFill(this.color);
		this.el.graphics.rect(0, 0, this.width, this.height);
		this.el.graphics.endFill();
		this.el.graphics.closePath();
	}
	
	destroy () {
		super.destroy();
	}
}