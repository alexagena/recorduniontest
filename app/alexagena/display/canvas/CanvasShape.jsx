//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasShape
 *
 */
export default class CanvasShape extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//create element
		this.el = new createjs.Shape();
	}

	// Public
	//-------------------------------------------------------
	create () {

	}

	destroy () {
		super.destroy();
	}
}