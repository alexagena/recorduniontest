//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';
import CanvasArc from './CanvasArc.jsx';

/*
 * CanvasArea
 *
 */
export default class CanvasArea extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//create element
		this.el = new createjs.Shape();
		this.container = new createjs.Container();

		//private
		this._alpha = 1;
		this._color = "#000";
		this._balls = [];
		this._ground = 0;
		this._vertices = [];
		this._ballRadius = 4;
	}

	// Getters and Setters
	//-------------------------------------------------------
	static get BALL_OVER () {return 'CanvasAreaBallOver';} 
	static get BALL_OUT () {return 'CanvasAreaBallOut';} 

	get color () {return this._color;}
	set color (value) {this._color = value;}

	get fill () {return this._fill;}
	set fill (value) {this._fill = value;}

	get alpha () {return this._alpha;}
	set alpha (value) {this._alpha = value;}

	get ground () {return this._ground;}
	set ground (value) {this._ground = value;}

	// Public
	//-------------------------------------------------------
	create () {
		this.container.addChild(this.el);
	}

	startAt (point = {}) {
		this._startPoint = point;
	}

	getVertex (index) {
		if(!this._vertices[index]) {
			//ball
			this._balls[index] = new CanvasArc(this.props);
			this._balls[index].create();
			this._balls[index].index = index;
			this._balls[index].radius = this._ballRadius;
			this._balls[index].endAngle = Math.PI * 2;
			this._balls[index].on(BaseCanvasItem.MOUSE_OUT, this._onBallOut.bind(this));
			this._balls[index].on(BaseCanvasItem.MOUSE_OVER, this._onBallOver.bind(this));
			this.container.addChild(this._balls[index].container);
			//vertex
			this._vertices[index] = {x: 0, y: 0, dot: true};
		}
		return this._vertices[index];
	}

	getVertices () {
		return this._vertices;
	}

	removeVertex (start, length) {
		length = this._vertices.length - start;
		//splice vertex
		this._vertices.splice(start, length);
		//remove unused balls
		for(let i = start; i < start + length; i++) {
			this._balls[i].hide(0, 1, true);
		}
	}

	draw () {
		this.el.graphics.clear();
		this.el.graphics.setStrokeStyle(2);
		this.el.graphics.beginStroke(this.color);
		this.el.alpha = this.alpha;

		if(this.fill) {
			this.el.graphics.beginFill(this.fill);
		}

		//draw vertex
		for(let i = 0; i < this._vertices.length; i++) {
			let ball = this._balls[i];
			var point = this._vertices[i];

			if(point && point.x && point.y) {
				ball.x = point.x;
				ball.y = point.y;
				ball.el.alpha = point.dot ? 1 : 0;
				ball.color = this.color;
				ball.draw();
				this.el.graphics.lineTo(point.x, point.y);
			}
		}

		//close path
		if(this._startPoint && point) {
			this.el.graphics.lineTo(point.x, this._startPoint.y);
			this.el.graphics.lineTo(this._startPoint.x, this._startPoint.y);
		}
		
		this.el.graphics.endFill();
		this.el.graphics.endStroke();
		this.props.stage.canUpdate = true;
	}

	hide (p_delay = 0, p_factor = 1) {
		for(var j = 0; j < this._vertices.length; j++) {
			let vertex = this._vertices[j];
			TweenMax.to(vertex, 0.5, {
				y: this.ground,
				ease: Quart.easeInOut,
				onUpdateParams: [j == this._vertices.length - 1],
				onUpdate:(isLast)=>{
					isLast && this.draw();
				}
			});
		}
	}

	destroy () {
		super.destroy();
		this.removeVertex(0);
	}

	selectBall (ball) {
		TweenMax.to(ball, 0.3, {
			radius: this._ballRadius * 2,
			ease: Back.easeOut,
			onUpdate:()=>ball.draw()
		});
	}

	deselectBall (ball) {
		TweenMax.to(ball, 0.3, {
			radius: this._ballRadius,
			ease: Back.easeOut,
			onUpdate:()=>ball.draw()
		});
	}

	// Private
	//-------------------------------------------------------
	_onBallOver (e) {
		let ball = e.target;
		this.trigger(CanvasArea.BALL_OVER, ball);
	}

	_onBallOut (e) {
		let ball = e.target;
		this.trigger(CanvasArea.BALL_OUT, ball);
	}
}