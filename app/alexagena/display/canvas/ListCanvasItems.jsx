// third party
import React from 'react';
import ReactDOM from 'react-dom';
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * ListCanvasItems
 *
 */
export default class ListCanvasItems extends EventDispatcher {
	
	constructor (props = {}) {
		//call parent
		super();
		
		//params
		this.props = {...ListCanvasItems.defaultProps, ...props};
		this.canvas = this.props.stage.canvas;
		this.parent = this.canvas.parentNode;
		this.container = new createjs.Container();
		this.data = null;
		this.items = [];
		this.dataChanged = false;
	}

	static get defaultProps () {
		return {
			stage: null,
			debug: false
		}
	}
	
	// Public
	//-------------------------------------------------------
	create () {
		
	}
	
	update (data, list) {
		this.dataChanged = (data != this.data) || !this.data;
		if(data.list) {
			this.data = data;
			this.list = list ? list : data.list;
			this._createItems(this.list);
		}
	}
	
	show () {
		
	}
	
	hide (p_delay = 0, p_factor = 1, p_destroy = false) {
		if(p_destroy) {
			this.destroy();
		}
	}
	
	destroy () {
		for(let i = 0; i < this.items.length; i++) {
			this.destroyItem(this.items[i], i);
		}
	}

	resize () {
		
	}

	// Protected
	//-------------------------------------------------------
	beforeCreateItems (loopData = {}) {
		return loopData;
	}
	
	createItem () {

	}

	updateItem (item, data, i, loopData) {

	}

	destroyItem (item, i, loopData) {

	}

	// Private
	//-------------------------------------------------------
	_createItems (list = []) {
		let newList = [];
		let loopData = this.beforeCreateItems({list});
		let loopLength = Math.max(list.length, this.items.length);

		//create items
		for(let i = 0; i < loopLength; i++) {
			let data = list[i];
			let item = this._getItem(i);
			
			//if dont have correspondent data, destroy item
			if(!data) {
				this.destroyItem(item, i, loopData);
			}
			//if dont have correspondent item, create new
			else {
				this.updateItem(item, data, i, loopData);
				newList.push(item);
			}
		}
		
		//cache and draw
		this.items = newList;
	}

	_getItem (i) {
		if(this.items[i]) {
			return this.items[i];
		}
		else {
			let item = this.createItem(i);
			this.items[i] = item;
			return this.items[i];
		}
	}
}