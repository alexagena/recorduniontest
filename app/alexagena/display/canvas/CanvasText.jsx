//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasText
 *
 */
export default class CanvasText extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);
		
		//create element
		this.el = new createjs.Text();
		this.container = new createjs.Container();
		
		//private vars
		this._text = "";
		this._fontSize = "";
		this._fontFamily = "";
		this._width = 0;
		this._height = 0;
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	get text () {return this._text;}
	set text (value) {
		this._text = this.el.text = value;
		this._updateSize();
	}
	
	get fontSize () {return this._fontSize;}
	set fontSize (value) {
		this._fontSize = value;
		this._updateFontProp();
		this._updateSize();
	}
	
	get fontFamily () {return this._fontFamily;}
	set fontFamily (value) {
		this._fontFamily = value;
		this._updateFontProp();
	}
	
	get width () {return this._width;}
	get height () {return this._height;}
	
	// Public
	//-------------------------------------------------------
	create () {
		this.x = 0;
		this.y = 0;
		this.container.addChild(this.el);
	}
	
	setAnglePosition (angle, radius, margin = 0.1) {
		this.x = Math.cos(angle) * (radius + (radius * margin) + (this.width / 2));
		this.y = Math.sin(angle) * (radius + (radius * margin) + (this.height / 2));
	}
	
	resize () {

	}
	
	destroy () {
		this.el.text = "";
		super.destroy();
	}
	
	// Private
	//-------------------------------------------------------
	_updateFontProp () {
		if(typeof(this._fontSize) == "number") {
			this._fontSize = this._fontSize + "px";
		}
		this.el.font = [this._fontSize, this._fontFamily].join(' ');
	}
	
	_updateSize () {
		if(!this.text) {
			this._width = 0;	
			this._height = 0;
		} 
		else {
			this._width = this.el.getMeasuredWidth();
			this._height = this.el.getMeasuredHeight();
		}
	}
}