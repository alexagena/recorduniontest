//framework
import BaseCanvasItem from './BaseCanvasItem.jsx';

/*
 * CanvasPolygon
 *
 */
export default class CanvasPolygon extends BaseCanvasItem {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this._paths = [];

		//create element
		this.el = new createjs.Shape();
		this.container = new createjs.Container();
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	get paths () {return this._paths;}
	set paths (value) {this._paths = value;}

	// Public
	//-------------------------------------------------------
	create () {
		this.el.x = 0;
		this.el.y = 0;
		this.container.addChild(this.el);
	}

	draw () {
		this.el.graphics.clear();
		this.el.graphics.beginFill(this.color);
		this.el.graphics.beginStroke(this.color);
		this.el.graphics.setStrokeStyle(0.5);
		
		for(let i = 0; i < this.paths.length; i++) {
			let point = this.paths[i];
			this.el.graphics[i == 0 ? "moveTo" : "lineTo"](point.x, point.y);
		}
		
		this.el.graphics.endFill();
		this.el.graphics.endStroke();
		this.el.graphics.closePath();
	}
}