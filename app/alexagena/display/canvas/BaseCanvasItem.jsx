//framework
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * BaseCanvasItem
 *
 */
export default class BaseCanvasItem extends EventDispatcher {

	constructor(props = {}) {
		//call parent
		super();

		//params
		this.el = {};
		this.props = {...BaseCanvasItem.defaultProps, ...props};
		this.container = new createjs.Container();
	}
	
	static get defaultProps () {
		return {
			stage: null
		}
	}

	static get MOUSE_OUT () {return 'BaseCanvasItemMouseOut';} 
	static get MOUSE_OVER () {return 'BaseCanvasItemMouseOver';} 

	// Getters and Setters
	//-------------------------------------------------------
	get x () {return this.el.x;}
	set x (value) {this.el.x = value;}
	
	get y () {return this.el.y;}
	set y (value) {this.el.y = value;}
	
	get alpha () {return this.el.alpha;}
	set alpha (value) {this.el.alpha = value;}

	// Public
	//-------------------------------------------------------
	create () {
		
	}
	
	draw () {

	}

	// Animations
	//-----------------------
	show (p_delay = 0, p_factor = 1) {

	}

	hide (p_delay = 0, p_factor = 1, p_destroy = false) {
		if(p_destroy) {
			this.destroy();
		}
	}

	// Dispose
	//-----------------------
	destroy () {
		if(this.container.parent) {
			this.container.parent.removeChild(this.container);
		}
	}
}