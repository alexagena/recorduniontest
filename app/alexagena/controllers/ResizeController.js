//third party
import React from 'react';
import ReactDOM from 'react-dom';
//framework
import EventDispatcher from '~fr/events/EventDispatcher.js';

/*
 * ResizeController
 *
 */
class ResizeController extends EventDispatcher {
	
	constructor (props = {}) {
		//call parent
		super();

		//params
		this._timer;

		//bind
		this._resize = this._resize.bind(this);
		
		//start
		window.addEventListener("resize", this._resize);
	}

	get RESIZE () {return "ResizeControllerResize"}
	
	// Private
	//-------------------------------------------------------
	_resize () {
		this._lazyUpdate();
	}

	_lazyUpdate () {
		if(this._timer) {
			clearTimeout(this._timer);
			this._timer = null;
		}
		this._timer = setTimeout(()=>{
			this.trigger(this.RESIZE);
		}, 100);
	}
}

export default new ResizeController();