/*
 * LanguageController
 *
 */
class LanguageController {

	constructor () {
		//params
		this._currentLang = "def";
		this._dictionaries = {};
	}

	// Public
	//-------------------------------------------------------
	addDictionary (data) {
		if(typeof(data) == "object" && data.length) {
			for(let i = 0; i < data.length; i++) {
				let item = data[i];
				if(typeof(item.data) == "object" && item.name) {
					this._addDictionary(item.data, item.name);
				}
			}
		}
		else {
			this._addDictionary(data);
		}
	}

	setCurrentLang (lang = "def") {
		this._currentLang = lang;
	}
	
	tr (key) {
		//remove {}
		if(/^{.*}$/gi.test(key)) {
			key = key.replace(/^{|}$/gi, '');
		}
		//find key
		let lang = this._currentLang;
		let dict = this._dictionaries[lang] || this._firstDictionary();
		//result
		if(dict[key]) {
			key = dict[key];
		}
		return key;
	}

	// Private
	//-------------------------------------------------------
	_addDictionary (data, name = "def") {
		if(typeof(data) == "string") {
			data = JSON.parse(data);
		}
		this._dictionaries[name] = data;
	}

	_firstDictionary () {
		let key = Object.keys(this._dictionaries)[0];
		return this._dictionaries[key];
	}
}

export default new LanguageController();