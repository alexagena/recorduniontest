//third party
import React from 'react';
import ReactDOM from 'react-dom';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import EventDispatcher from '~fr/events/EventDispatcher.js';
import DisplayController from '~fr/display/DisplayController.jsx';
//styles
import styles from './FloatingBoxController.scss';

/*
 * FloatingBoxController
 *
 */
export default class FloatingBoxController extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this.newBoxes = [];
		this.timerUpdate = null;

		//state
		this.state.activeBoxes = {};
		this.addClass('floating-box-controller');
	}

	// Getters and Setters
	//-------------------------------------------------------
	static get REQUEST_CLOSE () {return "onRequestClose"}

	// React
	//-------------------------------------------------------
	render () {
		return (
			<div ref="wrapper" class={this.state.className}>
				{this.renderBoxes()}
			</div>
		);
	}

	renderBoxes () {
		return Object.keys(this.state.activeBoxes).map((key)=> {
			let item = this.state.activeBoxes[key];
			let {Component, props} = item;
			item.ref = "button" + props.key;
			return <Component ref={item.ref} {...props} />;
		});
	}

	componentDidUpdate(prevProps, prevState) {
		this._setupNewBoxes();
	}

	// Public
	//-------------------------------------------------------
	addBox ({Component, props, button}) {
		if(props.key) {
			//save key
			props.internalKey = props.key;
			//cache new items
			let item = arguments[0];
			this.newBoxes.push(item);
			//update boxes
			let activeBoxes = {...this.state.activeBoxes};
			activeBoxes[props.key] = item;
			this.setState({activeBoxes});
			return item;
		}
		else {
			throw new Error("key property is required");
		}
	}

	removeBoxByKey (removeKey) {
		for(let key in this.state.activeBoxes) {
			if(removeKey == key) {
				this.state.activeBoxes[key] = null;
				delete this.state.activeBoxes[key];
			}
		}
		this._lazyUpdate();
	}
	
	fixPosition (key) {
		if(this.state.activeBoxes[key]) {
			this._setBoxPosition(this.state.activeBoxes[key]);
		}
	}

	// Private
	//-------------------------------------------------------
	_setupNewBoxes () {
		for(let i = 0; i < this.newBoxes.length; i++) {
			let currBox = this.newBoxes[i];
			let currBoxRef = this.refs[currBox.ref];
			this._setBoxPosition(currBox);
			if(currBoxRef.onBoxCreated) {
				currBoxRef.onBoxCreated();
				currBox.onBoxCreated = null;
			}
		}
		this.newBoxes.length = 0;
	}

	_setBoxPosition (box) {
		var boxReact = this.refs[box.ref];
		var boxDOM = ReactDOM.findDOMNode(boxReact);
		var boxBounds = boxDOM.getBoundingClientRect();
		var baseBounds = box.button.getBoundingClientRect();
		var top = baseBounds.top + baseBounds.height;
		var left = baseBounds.left - (boxBounds.width - baseBounds.width) / 2;

		//prevent corner limits
		left = Math.min(left, app.container.offsetWidth - boxBounds.width);
		left = Math.max(0, left);
		
		//prevent bottom limit
		if((top + boxBounds.height + 30) >= window.innerHeight) {
			top = baseBounds.top - boxBounds.height;
			boxReact.positionMode = 'bottom';
		}
		else {
			boxReact.positionMode = 'top';
		}

		//set styles
		boxDOM.style.top = top + 'px';
		boxDOM.style.left = left + 'px';

		//fix arrow position
		if(boxReact.refs.markerArrow) {
			var arrowLeft = baseBounds.left - left + ((baseBounds.width - boxReact.refs.markerArrow.offsetWidth) / 2);
			boxReact.refs.markerArrow.style.left = arrowLeft + 'px';
		}
	}

	_lazyUpdate () {
		if(this.timerUpdate) {
			clearTimeout(this.timerUpdate);
			this.timerUpdate = null;
		}
		this.timerUpdate = setTimeout(()=>{
			this.setState({activeBoxes: this.state.activeBoxes});
		}, 50);
	}
}