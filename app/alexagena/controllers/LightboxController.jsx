//third party
import React from 'react';
import ReactDOM from 'react-dom';
//framework
import BaseDisplay from '~fr/display/BaseDisplay.jsx';
import EventDispatcher from '~fr/events/EventDispatcher.js';
import ResizeController from '~fr/controllers/ResizeController.js';
import DisplayController from '~fr/display/DisplayController.jsx';
//styles
import styles from './LightboxController.scss';

/*
 * LightboxController
 *
 */
export default class LightboxController extends BaseDisplay {
	
	constructor (props = {}) {
		//call parent
		super(props);

		//params
		this.lightboxOpts = {};
		
		//default class
		this.addClass('lightbox');

		//bind methods
		this.resize = this.resize.bind(this);
		this._onRequestClose = this._onRequestClose.bind(this);
		this._onBackgroundClicked = this._onBackgroundClicked.bind(this);
	}
	
	// Getters and Setters
	//-------------------------------------------------------
	static get REQUEST_CLOSE () {return "onRequestClose"}

	// React
	//-------------------------------------------------------
	render () {
		return (
			<div ref="wrapper" class={this.state.className}>
				<DisplayController ref="displayController" className="modals-wrapper" />
				<div ref="background" class="background" onClick={this._onBackgroundClicked}></div>
			</div>
		);
	}

	componentDidMount() {
		this.hide(0, 0);
		super.componentDidMount();
		ResizeController.on(ResizeController.RESIZE, this.resize);
	}
	
	componentWillUnmount () {
		ResizeController.off(ResizeController.RESIZE, this.resize);
	}

	// Public
	//-------------------------------------------------------
	change ({Component, props, options}) {
		this.lightboxOpts = Object.assign(this._getLightboxOptions(), options);
		//add listeners
		props[LightboxController.REQUEST_CLOSE] = this._onRequestClose;
		//add to controller
		this.refs.displayController.change(Component, props);
	}

	show (p_delay = 0, p_factor = 1) {
		this.refs.wrapper.style.display = '';
		TweenMax.to(this.refs.wrapper, 0.4 * p_factor, {
			opacity: 1,
			ease: Quart.easeInOut,
			delay: p_delay * p_factor
		});
	}

	hide (p_delay = 0, p_factor = 1) {
		if(this.refs.displayController.currentPage) {
			this.refs.displayController.removeContent(()=> {
				this._hide(p_delay, p_factor);
			});
		}
		else {
			this._hide(p_delay, p_factor);
		}
	}

	resize () {
		let currentPage = this.refs.displayController.currentPageRef;
		currentPage && currentPage.resize();
	}
	
	// Private
	//-------------------------------------------------------
	_hide (p_delay = 0, p_factor = 1) {
		TweenMax.to(this.refs.wrapper, 0.4 * p_factor, {
			opacity: 0,
			ease: Quart.easeInOut,
			delay: p_delay * p_factor,
			onComplete: ()=> {
				this.refs.wrapper.style.display = 'none';
			}
		});
	}

	_getLightboxOptions () {
		return {
			closeKill: false,
			backgroundClose: true
		}
	}

	_onBackgroundClicked () {
		if(this.lightboxOpts.backgroundClose) {
			this.hide();
		}
	}

	_onRequestClose () {
		this.hide();
	}
}