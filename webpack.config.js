const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PATH_APP = path.join(__dirname, "app");
const PATH_DIST = path.join(__dirname, "dist");
const PATH_PROJ = path.join(PATH_APP, "project");
const NODE_PATH = path.join(__dirname, "node_modules");
const is_production = process.env.NODE_ENV;

let appEntry = [
	'webpack-dev-server/client?http://0.0.0.0:8080',
	'webpack/hot/only-dev-server'
];

if (is_production) {
	appEntry = [
		'react',
		'react-dom'
	];
}

let preloaderEntry = appEntry.slice();
	preloaderEntry.push('./app/project/preloader.jsx');

let mainEntry = appEntry.slice();
	mainEntry.push('./app/project/main.jsx');

let config = {
	entry: {
		'babel-polyfill': ['babel-polyfill'],
		preloader: preloaderEntry,
		main: mainEntry
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, PATH_DIST),
		publicPath: ''
	},
	devtool: is_production ? '' : 'source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				use: 'babel-loader',
				exclude: [
					path.resolve(__dirname, 'node_modules')
				]
			},
			{
				test: /\.(scss|css)$/,
				use: [
					'style-loader', 
					{
						loader: 'css-loader',
						options: {
							modules: false,
							importLoaders: 1,
							sourceMap: true
						}
					}, 
					'group-css-media-queries-loader',
					'resolve-url-loader',
					'sass-loader?sourceMap'
				]
			},
			{
				test: /\.(png|jpg|gif|ico)$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'images/'
					}
				}],
			}
		],
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new HtmlWebpackPlugin({
			filename: path.resolve(PATH_DIST, 'index.html'),
			template: path.resolve(PATH_PROJ, "template.ejs"),
			title: 'Record Union',
			appMountId: 'app-root-container',
			"files": {
				"js": [
					path.resolve(PATH_DIST, "preloader.bundle.js"),
					path.resolve(PATH_DIST, "main.bundle.js")
				],
				"chunks": {
					"preloader": {
						"entry": path.resolve(PATH_DIST, "preloader.bundle.js"),
						"css": []
					},
					"main": {
						"entry": path.resolve(PATH_DIST, "main.bundle.js"),
						"css": []
					}
				}
			},
			excludeChunks: ['main']
		})
	],
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			assets: path.resolve(__dirname, './src/assets/'),
			"~": PATH_APP,
			"~fr": PATH_APP + "/alexagena",
			"~pr": PATH_APP + "/project",
			"~md": PATH_APP + "/materialdesign"
		}
	}
};

if (is_production) {
	config.plugins.push(
		new UglifyJSPlugin({
			sourceMap: true
		}),
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		})
	);
} else {
	config.devServer = {
		contentBase: path.resolve(__dirname, 'dist'),
		host: '0.0.0.0',
		historyApiFallback: true,
		hot: true,
		inline: true
	};
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;